
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!------------------------------------------------------------------
SUBROUTINE drhoiu_by_dv(u, ndim, m, dvg)
 !
 ! This routine computes results of applying Xo or X_rpa at given
 ! imaginary frequency iu to m perturbation potentials. Whether the
 ! calculation is for Xo or X_rpa is dertermined by the control
 ! variables in the routine solving linear systems. Input potentials
 ! dvg are in G-space and become induced densites on exit.
 !
 USE kinds,         ONLY : DP
 USE lsda_mod,      ONLY : nspin
 USE gvect,         ONLY : ngm, nl
 USE fft_base,      ONLY : dfftp
 USE fft_interfaces,ONLY : fwfft, invfft
 USE control_acfdt, ONLY : dvgenc
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
 REAL(DP), INTENT(IN) :: u
 ! input: immagine frequency
 INTEGER, INTENT(IN) :: ndim, m
 ! input: dimension of the arrays for potential and charge density
 ! input: number of pot. 
 COMPLEX(DP), INTENT(INOUT) :: dvg(ndim,m)
 ! input: purturbed potentials in G-space
 ! output: induced densities in G-space
 !
 ! local variables
 !
 COMPLEX(DP), ALLOCATABLE :: aux(:)
 ! auxiliary arrays for FFT
 INTEGER :: i
 ! counter
 !
 ALLOCATE ( aux(dfftp%nnr) )
 !
 ! various checks
 !
 IF ( ndim > ngm ) CALL errore('drhoiu_by_dv', 'ndim too large', abs(ndim))
 IF ( ndim <= 0  ) CALL errore('drhoiu_by_dv', 'ndim negative!', abs(ndim))
 !
 ! Loop over potentials
 ! Xo|dvg> or X|dvg> is computed in R-space
 !
 DO i = 1, m
    !
    ! first bring dvg(:,i) to R-space
    aux(:) = ZERO; aux(nl(1:ndim)) = dvg(1:ndim,i)
    !aux(:) = ZERO; aux(nl(igq(1:ndim))) = dvg(1:ndim,i)
    CALL invfft ('Dense', aux, dfftp)
    ! 
    ! solve the linear system to obtain the density response
    dvgenc(:,1,1) = aux(:); dvgenc(:,:,2) = ZERO
    IF (nspin==2) dvgenc(:,nspin,1) = dvgenc(:,1,1)
    CALL solve_linter_iu ( u )
    IF (nspin==2) dvgenc(:,1,2) = dvgenc(:,1,2) + dvgenc(:,nspin,2)
    !
    ! go back to G-space
    aux(:) = dvgenc(:,1,2)
    CALL fwfft ('Dense', aux, dfftp)
    !dvg(1:ndim,i) = aux(nl(igq(1:ndim)))
    dvg(1:ndim,i) = aux(nl(1:ndim))
    ! 
 ENDDO
 !
 DEALLOCATE ( aux )
 !
 RETURN
 !
END SUBROUTINE drhoiu_by_dv
