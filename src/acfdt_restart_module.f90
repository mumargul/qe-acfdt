!
! Copyright (C) 2009 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define DEBUG
!----------------------------------------------------------------------
subroutine read_exxrpa_oep_status (iter_saved) 
  !--------------------------------------------------------------------
  !
  !	This routine is a driver for checking status running of OEP.
  !     First: it opens a file .recOEP that stores running status.
  !     Second: it checks if the OEP is started from_scratch or restart
  !     
  USE kinds,                ONLY : DP
  USE io_files,             ONLY : seqopn 
  USE io_global,            ONLY : stdout, ionode, ionode_id
  USE mp,                   ONLY : mp_bcast
  USE acfdt_scf,            ONLY : oeprec, oep_recover, oep_restart_flag
  USE mp_global,            ONLY : intra_image_comm
  !
  implicit none
  !
  integer, intent(out) :: iter_saved
  !
  ! I/O variables
  !
  logical :: exst
  !
  if ( ionode ) then   
    call seqopn (oeprec, 'recOEP', 'unformatted', exst)
    if (exst .and. oep_recover ) then
      write(stdout,'(/,7x,''.... OEP CALCULATION IS RESTARTING ....'')')
      rewind(oeprec); read(oeprec) iter_saved
      oep_restart_flag = .true. 
    elseif (oep_recover .and. .not.exst) then
       write(stdout,'(/,7x,''.... OEP RESTARTING CAN NOT READ, RESTARTING FROM SCRATCH ANYWAY  ....'')')
       iter_saved = 0
       oep_restart_flag = .false.
    else
       write(stdout,'(/,7x,''.... OEP CALCULATION IS STARTING ....'')')
       iter_saved = 0
       oep_restart_flag = .false.
    endif
 endif
 ! 
 ! distribute to every nodes
 ! 
#if defined __MPI
   call mp_bcast(iter_saved, ionode_id, intra_image_comm) ! NsC communicator added... CHECK!!!
   call mp_bcast(oep_restart_flag, ionode_id, intra_image_comm )  ! NsC added the communicator... CHECK!!!!
#endif
  !
  ! here, oep_restart_flag variable used to handle the later functions
  !
  return
  !
end subroutine read_exxrpa_oep_status 
!
!----------------------------------------------------------------------
subroutine save_exxrpa_oep_status (iter)
  !--------------------------------------------------------------------
  !
  USE io_global,            ONLY : stdout, ionode, ionode_id
  USE control_acfdt,        ONLY : unitchi0, chi0rec, inverted_chi  
  USE acfdt_scf,            ONLY : oeprec, iter_save, iter_oep, etot_old, &
                                   line_search, print_result, &
                                   poly_search, lambda_opt, save_norm_1, & 
                                   save_norm_2, oep_restart_flag, oep_method

  !
  implicit none
  !
  ! I/O variables
  integer, intent(in) :: iter
  !
  logical :: opnd
  !
  oep_restart_flag = .false.
  !
  ! save status of the OEP calculation
  !
  if(oep_method == 1 ) then   ! for the case of oep_method = 1
    !
    if ( ionode ) then
      rewind(oeprec); write(oeprec) iter
    endif
    !
  endif
  !
  if(oep_method .gt. 1 ) then ! for the case of oep_method = 2; 3; 4
    !
    if ( ionode ) then
      rewind(oeprec); write(oeprec) iter, iter_oep, etot_old, line_search, &
                              poly_search, print_result, lambda_opt, &
                              save_norm_1, save_norm_2
    endif
    !
  endif
  !
  !
  ! close file of chi0 status, if any.
  !
  if (inverted_chi) then
     if ( ionode ) then
       inquire( unit = unitchi0, opened = opnd )
       close( unit = unitchi0, status = 'delete' )
       inquire( unit = chi0rec, opened = opnd )
       close( unit = chi0rec, status = 'delete' )
     endif
  endif
  !
  return
  !
end subroutine save_exxrpa_oep_status
!
!----------------------------------------------------------------------
subroutine close_exxrpa_oep_status ()
  !--------------------------------------------------------------------
  !
  USE io_global,            ONLY : stdout, ionode, ionode_id
  USE acfdt_scf,            ONLY : oeprec 
  !
  implicit none
  !
  ! I/O variables
  !
  logical :: opnd
  !
  if ( ionode ) then
     inquire( unit = oeprec, opened = opnd )
     close  ( unit = oeprec, status = 'keep' )
  endif
  !
  return
  !
end subroutine close_exxrpa_oep_status
!
!----------------------------------------------------------------------------
SUBROUTINE restart_oep_acfdt ( stopped_iter )
  !----------------------------------------------------------------------------
  !
  ! ... This routine initializes the xc potential for reasting OEP runs.
  !
  USE kinds,                ONLY : DP
  USE io_global,            ONLY : stdout
  USE lsda_mod,             ONLY : lsda, nspin
  USE fft_base,             ONLY : dfftp
  USE scf,                  ONLY : rho, rho_core, rhog_core, v, vltot 
  USE ener,                 ONLY : ehart, etxc, vtxc, epaw
  USE control_acfdt,        ONLY : delta_vxc, vc_init, vx_init, etx_init, &
                                   vtx_init, etc_init, vtc_init, save_rho 
  USE acfdt_scf,            ONLY : vrs_save,oep_recover,oep_restart_flag, &
                                   stopped_first_iter
!!!!
  USE wavefunctions_module, ONLY : evc, psic
!!!!
  !
  IMPLICIT NONE
  !
  INTEGER, INTENT (INOUT)  :: stopped_iter
  INTEGER  :: is
  !
  ! Here, we pass the old rho into save_rho, that will be used later
  ! in OEP checking convergency. 
  ! If the calculation is from LDA/GGA; rho is density of LDA/GGA
  ! If the calculation is restarted; rho is density of OEP, that has been already read in read_file(). 
  !
  save_rho(:) = 0.0_DP; save_rho(:) = rho%of_r(:,nspin)
  !
  stopped_iter = 0
  !
  ! here, we declare the status of oep running... 
  ! then read the informations about the stopped step, if any, that was saved in previous run;
  !
  CALL read_exxrpa_oep_status (stopped_iter)
  !
  IF (.NOT.oep_recover) RETURN
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! here we recalculate xc potential with restart density,
  ! then this potential will be subtracted in order to cancel with v%of_rho 
  ! that has been calculated in routine read_file(). This part should be removed in future
  ! by call enforce_dft_exxrpa()
  !  
  ! if job is corrupted druring the first OEP step, when it restarts, xc potential is computed with
  ! LDA/GGA functionals, and one does not have to use this treatment
  !
IF (.not.stopped_first_iter) THEN 
  !
  ! 
  CALL vxc_init( rho, rho_core, rhog_core, &
            etx_init, vtx_init, etc_init, vtc_init, vx_init, vc_init )
  !
  DO is = 1, nspin
    v%of_r (:,is) = v%of_r (:,is) - (vx_init(:,is) + vc_init(:,is))
  ENDDO
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! pass the save_vrs to delta_vxc variable
  !
  delta_vxc(:) = 0.0_DP; delta_vxc(:) = vrs_save(:, nspin)
  !
  ! re-initialize vltot
  !
  call setlocal()
  !
  ! ... compute the potential and store it in vr
  !
  write(stdout,*)"vltot in restart_oep_acfdt"
  write(stdout,'(4F21.11)')vltot(1:12)
  write(stdout,*)"density in restart_oep_acfdt"
  write(stdout,'(4F21.11)')rho%of_r(1:12,nspin)
  write(stdout,*)"wave function in restart_oep_acfdt"
  write(stdout,'(4F21.11)')evc(1:12,nspin)
  !
  ! ... add delta_vxc term to vltot
  !
  vltot(:) = vltot(:) + delta_vxc(:)
  !
  ! vltot(:) will be reseted to local potential via set_vrs() routine
  ! that will be done in phq_setup() routine.
  !
ENDIF
  !
  ! here, we have to reset restart = false to prevent restart again in next oep steps
  oep_recover = .false.
  !
  ! now, we can deallocate vrs_save variable
  DEALLOCATE (vrs_save)
  !
  RETURN
  !
END SUBROUTINE restart_oep_acfdt


