!
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!
subroutine nonloc_dfpt_rpax ( )
  !--------------------------------------------------------------------
  !
  !     This subroutine is a driver for calculation of variational density  
  !     induced by a pertubed nonlocal potential, fock operator. 
  !     The following steps are performed:
  !
  !      1) Calculate Vnonloc|psi> 
  !      2) Proceduce as well as DPFT problem to estimate |dn>
  !
  USE kinds,                 ONLY : DP
  USE ions_base,             ONLY : nat
  USE io_global,             ONLY : stdout, ionode
  USE io_files,              ONLY : prefix
  USE check_stop,            ONLY : check_stop_now
  USE wavefunctions_module,  ONLY : evc, psic
  USE cell_base,             ONLY : tpiba2
  USE klist,                 ONLY : lgauss, nkstot, wk, xk, ngk, igk_k
  USE lsda_mod,              ONLY : lsda, nspin, current_spin, isk
  USE fft_base,              ONLY : dffts, dfftp
  USE fft_interfaces,        ONLY : fwfft, invfft
  USE gvect,                 ONLY : g, gstart, nl
  USE gvecs,                 ONLY : doublegrid, nls
  USE becmod,                ONLY : becp, calbec
  USE wvfct,                 ONLY : npwx, nbnd, et, current_k
  USE uspp_param,            ONLY : nhm
  USE control_lr,            ONLY : alpha_pv, lgamma, nbnd_occ
  USE control_ph,            ONLY : nmix_ph, tr2_ph, lgamma_gamma, convt, &
                                    alpha_mix, ldisp, rec_code 
  USE uspp,                  ONLY : nlcc_any
  USE units_ph,              ONLY : iudwf, lrdwf, iuwfc, lrwfc
  USE eqv,                   ONLY : dvpsi, dpsi, evq
  USE qpoint,                ONLY : nksq, ikks, ikqs
  ! used oly to write the restart file
  USE uspp,                  ONLY : nkb, vkb, okvan  
  USE mp,                    ONLY : mp_sum
  USE mp_global,             ONLY : intra_pool_comm, inter_pool_comm 
  USE exx,                   ONLY : vexx 
  USE control_acfdt,         ONLY : dvgenc, dpsi_nl, dpsiq_nl,  exxvx
  USE noncollin_module,      ONLY : noncolin, npol, nspin_mag
  USE paw_variables,        ONLY : okpaw
  USE buffers,              ONLY : get_buffer
  !
  implicit none
  !
  integer, parameter :: npe = 1
  !
  complex(DP), allocatable :: drhoscf (:,:,:)
!  complex(DP), allocatable :: h_diag (:,:,:), etiu (:,:,:)
!  ! h_diag: diagonal part of the Hamiltonian +/- iu
!  ! etiu  : eigenvalues +/- iu
  real(DP) , allocatable :: rh_diag (:,:)
  ! rh_diag: diagonal part of the Hamiltonian
  real(DP) :: thresh, anorm, dr2
  ! thresh: convergence threshold
  ! anorm : the norm of the error
  ! dr2   : self-consistency error
  real(DP) :: dos_ef, weight
  ! dos_ef: DOS at Fermi energy (in calculation for a metal)
  ! weight: weight of k-point     
  complex(DP), allocatable, target :: dvscfin(:,:,:)
  ! change of the scf potential 
  complex(DP), pointer :: dvscfins (:,:,:)
  ! change of the scf potential (smooth part only)
  ! NsC Start 
   real(DP), allocatable, target :: vxlocal(:)
   real(DP), pointer :: vxlocals(:)
   COMPLEX(DP), ALLOCATABLE :: dpsi_aux(:,:)
   REAL(DP), EXTERNAL :: DDOT
  ! NsC end
  complex(DP), allocatable :: drhoscfh (:,:,:), dvscfout (:,:,:)
  ! change of rho / scf potential (output)
  ! change of scf potential (output)
  complex(DP), allocatable :: ldos (:,:), ldoss (:,:),&
       dbecsum (:,:,:,:), aux1 (:) 
  ! Misc work space
  ! ldos : local density of states af Ef
  ! ldoss: as above, without augmentation charges
  ! dbecsum: the derivative of becsum
  REAL(DP), allocatable :: becsum1(:,:,:)
  !
  complex(DP) :: ZDOTC
  ! the scalar product function

  logical :: conv_root,  & ! true if linear system is converged
             iuzero,     & ! true if freq uu==0
             exst,       & ! used to open the recover file
             lmetq0        ! true if xq=(0,0,0) in a metal

  integer :: kter,       & ! counter on iterations
             iter0,      & ! starting iteration
             ibnd, jbnd, & ! counter on bands
             iter,       & ! counter on iterations
             lter,       & ! counter on iterations of linear system
             lintercall, & ! average number of calls to cgsolve_all
             ik, ikk,    & ! counter on k points
             ikq,        & ! counter on k+q points
             ig,         & ! counter on G vectors
             ir,         & ! counter on mesh points
             is,         & ! counter on spin polarizations
             ios           ! integer variable for I/O control
  !           mode          ! mode index

  integer :: irr
  integer :: npw, npwq

  real(DP) :: tcpu, get_clock ! timing variables

  external cch_psi_all, ccg_psi
  external ch_psi_all,  cg_psi
  !
  call start_clock ('solve_linter_iu')
  !
  !
  allocate( rh_diag(npwx, nbnd) )

  allocate (dvscfin ( dfftp%nnr, nspin , npe))    
  ! NsC Start
   allocate ( vxlocal(dfftp%nnr) )
   allocate ( dpsi_aux(npwx,nbnd) )
  ! NsC end
  if (doublegrid) then
     allocate (dvscfins ( dffts%nnr, nspin , npe))    
     ! NsC Satrt
      allocate ( vxlocals(dffts%nnr) )
     ! NsC End
  else
     dvscfins => dvscfin
     vxlocals => vxlocal
  endif
  allocate (drhoscf (dfftp%nnr, nspin , 1  ))
  allocate (drhoscfh(dfftp%nnr, nspin , npe))    
  allocate (dvscfout(dfftp%nnr, nspin , npe))    
  allocate (dbecsum ((nhm * (nhm + 1))/2 , nat , nspin , npe))    
  allocate (aux1 (dffts%nnr) ) 
  !
  ! if q=0 for a metal: allocate and compute local DOS at Ef
  !
  lmetq0 = lgauss.and.lgamma
  if (lmetq0) then
     allocate ( ldos ( dfftp%nnr, nspin) )    
     allocate ( ldoss( dffts%nnr, nspin) )    
     allocate (becsum1 ( (nhm * (nhm + 1))/2 , nat , nspin_mag))
     call localdos_paw ( ldos , ldoss , becsum1, dos_ef )
     IF (.NOT.okpaw) deallocate(becsum1)
  endif
  !
  !   The outside loop is over the iterations
  !   !!!!!!! ACTUALLY NO LOOP HERE !!!!!!!!! 
  !
  do kter = 1, 1 
     iter = kter + iter0
     !
     lintercall = 0
     drhoscf(:,:, 1) = (0.d0, 0.d0)
     dbecsum(:,:,:,:) = (0.d0, 0.d0)
     !
     !
!     if (nksq.gt.1) rewind (unit = iunigk)
     do ik = 1, nksq
!        if (nksq.gt.1) then
!           read (iunigk, err = 100, iostat = ios) npw, igk
!100        call errore ('solve_dviu', 'reading igk', abs (ios) )
!        endif
!        if (lgamma) npwq = npw
        ikk = ikks(ik)
        ikq = ikqs(ik)
        ! NsC >
        npw = ngk(ikk)
        npwq= ngk(ikq)
        ! < 
        current_k = ikq ! for RPA Ec with HF or hybrid functionals
        if (lsda) current_spin = isk (ikk)
!        if (.not.lgamma.and.nksq.gt.1) then
!           read (iunigk, err = 200, iostat = ios) npwq, igkq
!200        call errore ('solve_dviu', 'reading igkq', abs (ios) )
!        endif
        !
        ! reads unperturbed wavefuctions psi(k) and psi(k+q)
        !
        if (nksq.gt.1) then
!           call davcio (evc, lrwfc, iuwfc, ikk, - 1)
           call get_buffer (evc, lrwfc, iuwfc, ikk)  ! NsC from version 283
           ! if q/= 0 read from file evq wfct at k+q needed for dpsiq_nl (see below)
!           if (.not. lgamma)  call davcio (evq, lrwfc, iuwfc, ikq, - 1)
           if (.not. lgamma)  call get_buffer (evc, lrwfc, iuwfc, ikq) ! NsC from version 283
        endif
        !
        ! compute beta functions and kinetic energy for k-point ikq
        ! needed by h_psi, called by ch_psi_all, called by cgsolve_all
!        call init_us_2 (npwq, igkq, xk (1, ikq), vkb)
        CALL init_us_2 (npwq, igk_k(1,ikq), xk(1,ikq), vkb) 
        CALL g2_kin (ikq)
        ! compute preconditioning matrix h_diag used by cgsolve_all
        !
        CALL h_prec (ik, evq, rh_diag)
        !
!        ! compute the kinetic energy at k
!        !
!        do ig = 1, npw
!           g2kin (ig) = ( (xk (1,ik) + g (1, igk(ig)) ) **2 + &
!                          (xk (2,ik) + g (2, igk(ig)) ) **2 + &
!                          (xk (3,ik) + g (3, igk(ig)) ) **2 ) * tpiba2
!        enddo
!        !
!        !
!        rh_diag(:,:) = 0.d0
!        do ibnd = 1, nbnd_occ (ikk)
!           do ig = 1, npw
!              rh_diag(ig,ibnd) = 1.d0 / max(1.0d0,g2kin(ig)/eprec(ibnd,ik))
!           enddo
!        enddo
        !
        ! This is one difference from other version of PDFT
        !
        ! compute the right hand side of the linear system due to
        ! a nonlocal potential perturbation, dvscfin used as work space
        !
        ! Apply Fock operator on evc, 
        ! and orthogonalize to conduction manifold
        dvpsi(:,:) = (0.d0, 0.d0)
!        WRITE(*,*) 'nband    ', nbnd_occ(ikk)
        call vexx( npwx, npw, nbnd_occ(ikk), evc, dvpsi )
!        WRITE(*,*) 'NON LOCAL 1', dvpsi(1:3,1)
        !
        ! NsC Start: Bisogna aggiungere a dvpsi=Vx_NL|psi> il contributo -vx_L|psi>
        !            dvpsi = (Vx_Nl-vx_L)|psi>
         
!        WRITE(*,*) 
!        WRITE(*,*) 'nonloc start', dvpsi(1:3,1)
        
        vxlocal(:) = 0.D0 ;  vxlocal (:) = exxvx (:)
         IF (doublegrid) CALL interpolate(vxlocal(1),vxlocals(1),-1) 
!         WRITE(*,*) 'vxexx    ', vxlocals(1:3)
         DO ibnd = 1, nbnd_occ(ikk)
            aux1(:) = (0.D0, 0.D0)
            DO ig = 1, npw
               aux1(nls(igk_k(ig,ikk))) = evc(ig,ibnd)
            ENDDO
            CALL invfft ('Wave' , aux1, dffts)
            DO ir =1, dffts%nnr              !!! CALCOLA vxlocal(r)*psi(r)
               aux1(ir) = aux1(ir)*vxlocals(ir) 
            ENDDO
            CALL fwfft('Wave', aux1, dffts)
            DO ig = 1, npw        !!! CALCOLA dvpsi_NL - dvdps_L (il primo e' in dvpsi il secondo in aux1)
!               IF (ibnd == 1) WRITE(27,*)  REAL ( ( dvpsi(ig,ibnd) - aux1(nls(igk(ig))) )/evc(ig,ibnd) ), &
!                           AIMAG( ( dvpsi(ig,ibnd) - aux1(nls(igk(ig))) )/evc(ig,ibnd) ) !, REAL(evc(ig,ibnd)) , AIMAG(evc(ig,ibnd))
!               WRITE(*,*)
               dvpsi(ig, ibnd) = dvpsi(ig,ibnd) - aux1(nls(igk_k(ig,ikk)))
            ENDDO
!            write(*,*) 'dvpsi2' , dvpsi(1:3,ibnd)
         ENDDO
!        WRITE(*,*) 'NON LOCAL 2 expec ~ zero', dvpsi(1:3,1)
!        WRITE(*,*)
        ! NsC end
!        CALL orthogonalize( dvpsi, evq, ikk, ikq, dpsi, npwq, .false.)
        CALL orthogonalize( dvpsi, evc, ikk, ikk, dpsi, npw, .false.)
!        WRITE(28,*) 'dvdpsi ortho    ', dvpsi(:,1)
        !
        if (okvan) then
           call errore('solve_dviu', 'USPP not implemented yet', 1)
        endif
        !
        ! iterative solution of the linear system (H-eS)*dpsi=dvpsi,
        ! dvpsi=-P_c^+ (dvbare+dvscf)*psi , dvscf fixed.
        !
        thresh = tr2_ph
        !
        weight = wk (ikk); conv_root = .true.; dpsi(:,:)=(0.d0,0.d0)
        call cgsolve_all (ch_psi_all, cg_psi, et(1,ikk), dvpsi, dpsi, &
                          rh_diag, npwx, npwq, thresh, ik, lter, conv_root, &
                          anorm, nbnd_occ(ikk), 1)
        ! 
!        write(*,*) 'prima', DDOT(2*npw,evc,1,dpsi,1)
        dpsi_aux(:,:) = (0.D0, 0.D0)
        CALL orthogonalize( dpsi, evc, ikk, ikk, dpsi_aux, npw, .false.)
        dpsi(:,:) = -1.D0 * dpsi(:,:)
!        write(*,*) 'dopo', DDOT(2*npw,evc,1,dpsi,1)
        !
        !! NsC start: Save dpsi_nl on file if # of kpoints gt 1 else store it in memory
        if (nksq.gt.1) then
           call davcio ( dpsi, lrdwf, iudwf, ik+nksq+nksq, +1)
        else
           dpsi_nl (:,:) = dpsi (:,:)  
        endif
        !! NsC End
        !
        if (.not.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
                &              " solve_linter_iu: root not converged ",e10.3)') &
                &              ik , ibnd, anorm
        !
        ! the induced density 
        !
        call incdrhoscf (drhoscf(1,current_spin,1), weight, ik, &
                         dbecsum(1,1,current_spin,1), dpsi)
 
!!!!!!!!!======================================================!!!!!!!!!     
!!!!!!!!!      if q/=0 we need also dpsi_nl at k+q             !!!!!!!!!
!!!!!!!!!======================================================!!!!!!!!! 
        !
        IF ( .NOT. lgamma) THEN
           !
!           ! Kinetic energy at k+q
           CALL g2_kin (ikq)
           ! compute preconditioning matrix h_diag used by cgsolve_all
           !
           CALL h_prec (ik, evq, rh_diag)
           !
!           !
!           DO ig = 1, npwq
!              g2kin (ig) = ( (xk (1,ikq) + g (1, igkq(ig)) ) **2 + &
!                             (xk (2,ikq) + g (2, igkq(ig)) ) **2 + &
!                             (xk (3,ikq) + g (3, igkq(ig)) ) **2 ) * tpiba2
!           ENDDO
!           !
!           !
!           rh_diag(:,:) = 0.d0
!           DO ibnd = 1, nbnd_occ (ikq)
!              DO ig = 1, npwq
!                 rh_diag(ig,ibnd) = 1.d0 / max(1.0d0,g2kin(ig)/eprec(ibnd,ik))
!              ENDDO
!           ENDDO
           !
           ! This is one difference from other version of PDFT
           !
           ! compute the right hand side of the linear system due to
           ! a nonlocal potential perturbation, dvscfin used as work space
           !
           ! Apply Fock operator on evc, 
           ! and orthogonalize to conduction manifold
           !
           dvpsi(:,:) = (0.d0, 0.d0)

!!!!!!!!!!!!!===============================================================!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!  NB vexx routine does not do the work if we are at q /= GAMMA !!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!  must be changed                                              !!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!===============================================================!!!!!!!!!!!!!!!!!!!!!!

           CALL vexx( npwx, npw, nbnd_occ(ikk), evc, dvpsi )        !!!!!!!! NB vexx routine must be changed if evq is used
                                                                    !!!!!!!! See the rhs_dchi_rpax where something smilar is computed !!!!!
!           CALL vexx_rpax ( npwx, npwq, nbnd_occ(ikq), evq, dvpsi )
           !
           ! NsC Start: Bisogna aggiungere a dvpsi=Vx_NL|psi> il contributo -vx_L|psi>
           !            dvpsi = (Vx_Nl-vx_L)|psi>
           !
           vxlocal(:) = 0.D0 ;  vxlocal (:) = exxvx (:)
           IF (doublegrid) CALL interpolate(vxlocal(1),vxlocals(1),-1) 
!           WRITE(*,*) 'vxexx    ', vxlocals(1:3)
           DO ibnd = 1, nbnd_occ(ikq)
              aux1(:) = (0.D0, 0.D0)
              DO ig = 1, npwq
                 aux1(nls(igk_k(ig,ikq))) = evq(ig,ibnd)
              ENDDO
              CALL invfft ('Wave' , aux1, dffts)
              DO ir =1, dffts%nnr              !!! CALCOLA vxlocal(r)*psi(r)
                 aux1(ir) = aux1(ir)*vxlocals(ir) !!!!!! CONTROLLARE CHE LA GRIGLIA SIA LA STESSA
              ENDDO
              CALL fwfft('Wave', aux1, dffts)
              DO ig = 1, npwq        !!! CALCOLA dvpsi_NL - dvdps_L (il primo e' in dvpsi il secondo in aux1)
!                 IF (ibnd == 1) WRITE(27,*)  REAL ( ( dvpsi(ig,ibnd) - aux1(nls(igk(ig))) )/evc(ig,ibnd) ), &
!                             AIMAG( ( dvpsi(ig,ibnd) - aux1(nls(igk(ig))) )/evc(ig,ibnd) ) !, REAL(evc(ig,ibnd)) , AIMAG(evc(ig,ibnd))
!                 WRITE(*,*)
                 dvpsi(ig, ibnd) = dvpsi(ig,ibnd) - aux1(nls(igk_k(ig,ikq)))
              ENDDO
!              write(*,*) 'dvpsi2' , dvpsi(1:3,ibnd)
           ENDDO
!           WRITE(*,*) 'NON LOCAL 2', dvpsi(1:3,1)
!           WRITE(*,*)
          ! NsC end
           CALL orthogonalize( dvpsi, evq, ikk, ikq, dpsi, npwq, .false.)
!           WRITE(28,*) 'dvdpsi ortho    ', dvpsi(:,1)
           !
           if (okvan) then
              call errore('solve_dviu', 'USPP not implemented yet', 1)
           endif
           !
           ! iterative solution of the linear system (H-eS)*dpsi=dvpsi,
           ! dvpsi=-P_c^+ (dvbare+dvscf)*psi , dvscf fixed.
           !
           thresh = tr2_ph
           !
           weight = wk (ikk); conv_root = .true.; dpsi(:,:)=(0.d0,0.d0)
           call cgsolve_all (ch_psi_all, cg_psi, et(1,ikq), dvpsi, dpsi, &
                             rh_diag, npwx, npwq, thresh, ik, lter, conv_root, &
                             anorm, nbnd_occ(ikq), 1)
           !
           dpsi_aux(:,:) = (0.D0, 0.D0)
           CALL orthogonalize(dpsi, evq, ikk, ikq, dpsi_aux, npwq, .false.)
           dpsi(:,:) = -1.D0 * dpsi(:,:)
           !
           !! NsC start: Save dpsi on file if # of kpoints gt 1 else store it in memory
           if (nksq.gt.1) then
              call davcio ( dpsi, lrdwf, iudwf, ik+nksq+nksq+nksq, +1)
           else
              dpsiq_nl (:,:) = dpsi (:,:)  
           endif
           !! NsC End
           !
           if (.not.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
                &              " solve_linter_iu: root not converged ",e10.3)') &
                &              ik , ibnd, anorm
           !
        ENDIF   

     enddo ! on k-points
     !
     ! 
#if defined __MPI
     !
     !  The calculation of dbecsum is distributed across processors (see addusdbec)
     !  Sum over processors the contributions coming from each slice of bands
     !
     call mp_sum (dbecsum, intra_pool_comm)
     ! 
#endif
     !
     if (doublegrid) then
        do is = 1, nspin
           call cinterpolate (drhoscfh(1,is,1), drhoscf(1,is,1), 1)
        enddo
     else
        call ZCOPY (npe*nspin*dfftp%nnr, drhoscf, 1, drhoscfh, 1)
     endif
     !
     ! if q=0, make sure that charge conservation is guaranteed
     !
     if ( lgamma ) then
        psic(:) = drhoscfh(:, nspin, npe)
        CALL fwfft ('Dense', psic, dfftp)
        !CALL cft3 (psic, nr1, nr2, nr3, nrx1, nrx2, nrx3, -1)
        if ( gstart==2) psic(nl(1)) = (0.d0, 0.d0)
        CALL invfft ('Dense', psic, dfftp)
        !CALL cft3 (psic, nr1, nr2, nr3, nrx1, nrx2, nrx3, +1)
        drhoscfh(:, nspin, npe) = psic(:)
     endif
     !
     !    Now we compute for all perturbations the total charge and potential
     !
     !call addusddens (drhoscfh, dbecsum, irr, imode0, npe, 0)
#if defined __MPI
     !
     !   Reduce the delta rho across pools
     !
     call mp_sum (drhoscf, inter_pool_comm)
     !
     call mp_sum (drhoscfh, inter_pool_comm)
     !
#endif
     !
     !
     ! here we pass drhoscf to dvgenc to used as output
     !
     convt = .true.
     if ( convt ) then
        dvgenc(:,:,2) = drhoscfh(:,:,1) 
        goto 111
     endif
     !
  enddo ! loop over iteration
  !
  111 CONTINUE
  if (lmetq0) deallocate (ldoss)
  if (lmetq0) deallocate (ldos)
  deallocate (rh_diag)
  deallocate (aux1)
  deallocate (dbecsum)
  deallocate (drhoscf )
  deallocate (dvscfout)
  ! Nsc Start
  deallocate (vxlocal)
  deallocate (dpsi_aux)
  if (doublegrid) deallocate (vxlocals)
  ! Nsc End
  deallocate (drhoscfh)
  if (doublegrid) deallocate (dvscfins)
  deallocate (dvscfin)

  call stop_clock ('solve_linter_iu')
  return
end subroutine nonloc_dfpt_rpax
