
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!#define DEBUG 
!
!----------------------------------------------------------------------
SUBROUTINE init_depot ( nvec, dvg, iflag, iun )
  !--------------------------------------------------------------------
  !
  !    This routine is for manipulating Dielectric EigenPOTentials
  !    
  USE kinds,                ONLY : dp
  USE constants,            ONLY : tpi
  USE io_global,            ONLY : stdout, ionode, ionode_id
  USE mp_global,            ONLY : nproc_pool, me_pool, intra_pool_comm, root_pool, &
                                   npot => nimage, &
                                   my_pot_id => my_image_id, &
                                   me_pot => me_image, &
                                   inter_pot_comm => inter_image_comm
  USE gvect,                ONLY : ngm, g, ig_l2g, ngm_g
  USE mp_wave,              ONLY : splitwf, mergewf
  USE mp,                   ONLY : mp_bcast, mp_sum
  USE random_numbers,       ONLY : randy
  USE fft_base,             ONLY : dfftp
  USE control_acfdt,        ONLY : pot_collect, dvgg
  !
  !
  IMPLICIT NONE
  !
  ! I/O variables
  !
  INTEGER, INTENT(IN)  :: nvec
  ! input: number of potential to be generated
  COMPLEX(DP), INTENT(INOUT) :: dvg(ngm, nvec)
  ! input: converged eigenpotentials to write on disk
  ! output: trial potentials for diagonalization of response functions
  INTEGER, INTENT(IN)  :: iflag
  ! input: control variable
  ! iflag=0: generates random trial potentials 
  ! iflag>0: writes converged eigenpotentials to disk
  ! iflag<0: read converged eigenpotentials from disk
  INTEGER, OPTIONAL, INTENT(IN) :: iun
  ! input: unit of file
  !
  ! local variables
  !
  COMPLEX(DP), ALLOCATABLE :: dvg1(:)
  REAL(DP) :: rr, arg, norm(nvec) 
  INTEGER  :: ig, n
  ! counters
  REAL(DP), EXTERNAL :: DDOT
  !
  ! various checkings
  !
  IF (nvec .LE. 0) CALL errore('init_depot', 'nvec must be positive', 1)
  !
  IF ( iflag == 0 ) THEN
     !
     WRITE(stdout,'(/,7x,''Generate'',i5,2x,''random potentials'')') nvec
     !
#if defined DEBUG
     !
write(stdout,*)"me_pool = ", me_pool, "root_pool = ", root_pool, "ionode_id =", ionode_id, ionode
!stop
     ALLOCATE( dvg1(ngm_g) )
     DO n = 1, nvec
        !
        IF ( ionode ) THEN
        !IF ( me_pool == root_pool ) THEN
           DO ig = 1, ngm_g
              rr  = randy(); arg = tpi * randy()
              dvg1(ig) = cmplx( rr*cos( arg ), rr*sin( arg ) ) / &
                         ( g(1,ig)**2 + g(2,ig)**2 + g(3,ig)**2 + 1.D0 )
           ENDDO
           norm(1) = SQRT( DDOT( 2*ngm_g, dvg1(1), 1, dvg1(1), 1 ) )
           dvg1(:) = dvg1(:) / CMPLX( norm(1), 0.d0, kind=DP )
        ENDIF
        !
        CALL splitwf ( dvg(1:ngm,n), dvg1, ngm, ig_l2g, me_pool, nproc_pool, &
                       ionode_id, intra_pool_comm )
        !CALL splitwf ( dvg(1:ngm,n), dvg1, ngm, ig_l2g, me_pool, nproc_pool, &
        !               ionode_id, intra_pool_comm )
        !IF ( npot > 1 ) call mp_bcast ( dvg(1:ngm,n), me_pot, inter_pot_comm)
     ENDDO
     DEALLOCATE( dvg1 )
     !
#else
     !
!     IF ( my_pot_id == 0 ) THEN
        !
        ! only one group of processor generate random pots
        DO n = 1, nvec
           DO ig = 1, ngm
              rr  = randy(); arg = tpi * randy()
!              rr  = 1.0 ; arg = tpi * 0.01  ! NsC testing
              dvg(ig,n) = cmplx( rr*cos( arg ), rr*sin( arg ) ) / &
                          ( g(1,ig)**2 + g(2,ig)**2 + g(3,ig)**2 + 1.D0 )
           ENDDO
           norm(n) = DDOT( 2*ngm, dvg(1,n), 1, dvg(1,n), 1 )
        ENDDO
        !
#if defined __MPI
        CALL mp_sum(norm (1:nvec),  intra_pool_comm)
#endif
        !
        norm(1:nvec) = SQRT(norm(1:nvec))
        DO n = 1, nvec
           dvg(:,n) = dvg(:,n) / CMPLX(norm(n),0.d0,kind=DP)
        ENDDO
        !
!     ENDIF
     !
     ! send to other groups where dvg are zero using mp_sum tricks
!#if defined __MPI
!     CALL mp_sum( dvg(:,1:nvec), inter_pot_comm)
!#endif
     !
#endif
     !
  ELSE IF ( iflag < 0 ) THEN
     !
     WRITE(stdout,'(/,7x,''Read'',i5,2x,''potentials from disk'')') nvec
     !
     dvg(:,1:nvec) = (0.d0, 0.d0)
     IF ( pot_collect ) THEN
        DO n = 1, nvec
           IF ( ionode ) CALL davcio (dvgg, 2*ngm_g, iun, n, -1)
           CALL splitwf ( dvg(:,n), dvgg, ngm, ig_l2g, me_pool, nproc_pool, &
                          ionode_id, intra_pool_comm )
        ENDDO
#if defined __MPI
        ! send to other pot groups where dvg are zero using mp_sum trick
        CALL mp_sum( dvg(:,1:nvec), inter_pot_comm )
#endif
     ELSE
        DO n = 1, nvec
           CALL davcio (dvg(1,n), 2*ngm, iun, n, -1)
        ENDDO
     ENDIF
     !
  ELSE
     !
     WRITE(stdout,'(/,7x,''Write'',i5,2x,''potentials to disk'')') nvec
     !
     IF ( pot_collect ) THEN
        DO n = 1, nvec
           CALL mergewf ( dvg(:,n), dvgg, ngm, ig_l2g, me_pool, nproc_pool, &
                          ionode_id, intra_pool_comm )
           IF ( ionode ) CALL davcio (dvgg, 2*ngm_g, iun, n, +1)
        ENDDO
     ELSE
        DO n = 1, nvec
           CALL davcio (dvg(1,n), 2*ngm, iun, n, +1)
        ENDDO
     ENDIF
     !
  ENDIF
  !
  RETURN
  !
END SUBROUTINE init_depot
