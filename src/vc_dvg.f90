
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!------------------------------------------------------------------
SUBROUTINE update_pot( ndim, xq, dvr)
 !
 ! This routine computes results of applying Vc to a potential
 ! associated with vector q. Coulomb cutoff technique can be used
 !
 USE kinds,            ONLY : DP
 USE io_global,        ONLY : stdout
 USE constants,        ONLY : fpi, e2
 USE fft_base,         ONLY : dffts, dfftp
 USE fft_interfaces,   ONLY : fwfft, invfft
 USE cell_base,        ONLY : tpiba2, tpiba
 USE gvect,            ONLY : ngm, g, nl, gstart, nlm
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
 INTEGER, INTENT(IN) :: ndim
 ! input: dimension of the arrays for potential and charge density
 REAL(DP), INTENT(IN) :: xq(3)
 ! input: coordinates of q-vector
 REAL(DP), INTENT(INOUT) :: dvr(ndim)
 ! inout: density in R-space (potential is on exit)
 !
 ! local variables
 !
 INTEGER    :: i, ig
 REAL(DP)   :: qg, qg2, fac
 REAL(DP), PARAMETER :: const1 = 1.0_DP, const2 = 3.5_DP
 COMPLEX(DP):: sum_tmp
 COMPLEX(DP), ALLOCATABLE :: aux(:), dvg(:)
 ! density/potential in G-space
 !
 ! various checks
 !
 ALLOCATE ( aux(dfftp%nnr), dvg(ngm) )
 !
 IF ( ndim .GT. dfftp%nnr) CALL errore('vc_dvg', 'ndim too large', 1)
 IF ( ndim .LE. 0   ) CALL errore('vc_dvg', 'ndim negative!', 1)
 !
 !put dvg on the FFT grid
 aux(:) = ZERO; aux(:) = cmplx(dvr(:),0.0_DP)
 !go to G-space
 CALL fwfft ('Dense', aux, dfftp)
 dvg(1:ngm) = aux(nl(1:ngm))
 !
 DO ig = 1, ngm
    qg2 = (g(1,ig)+xq(1))**2 + (g(2,ig)+xq(2))**2 + (g(3,ig)+xq(3))**2
    IF (qg2 > 1.d-10) THEN 
       !fac = (e2*fpi)/(qg2*tpiba2)
       !fac = -((1+qg2*const1)**2)*(e2*fpi)/(const2*qg2*tpiba2)
       fac = -qg2
       dvg(ig) = dvg(ig) *cmplx(fac, 0.0_DP)
    ENDIF
 ENDDO
 !
 CALL projector(ngm, dvg)
 !put dvg on the FFT grid
 aux(:) = ZERO; aux(nl(1:ngm)) = dvg(1:ngm)
 !go to R-space
 CALL invfft ('Dense', aux, dfftp)
 dvr(:) = 0.0_DP; dvr(:) = dble(aux(:))
 !
 DEALLOCATE ( aux, dvg)
 !
 RETURN
 !
END SUBROUTINE update_pot
!
!------------------------------------------------------------------
SUBROUTINE projector( ndim, dv)
 !
 ! This routine solve projection of potential 
 ! on external sub-space Xo matrix. 
 !
 USE kinds,            ONLY : DP
 USE io_global,        ONLY : stdout
 USE mp,               ONLY : mp_sum
 USE mp_global,        ONLY : intra_pool_comm
 USE cell_base,        ONLY : omega
 USE constants,        ONLY : fpi, e2
 USE gvect,            ONLY : ngm, g, nl, gstart, nlm
 USE control_acfdt,    ONLY : iuchi0, depotchi0, neigvchi0, eigchi0, &
                              chi0_is_solved, neigv, depot, iundvr, lowest_eigchi0
 USE control_lr,       ONLY : lgamma 
 USE fft_base,         ONLY : dffts, dfftp
 USE fft_interfaces,   ONLY : fwfft, invfft
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
 INTEGER, INTENT(IN) :: ndim
 ! input: dimension of the arrays for potential in G-space
 COMPLEX(DP), INTENT(INOUT) :: dv(ndim)
 !
 ! Local variables
 !
 COMPLEX(DP), ALLOCATABLE :: aux(:), aux2(:)
 ! auxiliary arrays 
 COMPLEX(DP) :: zfac, sum_tmp
 INTEGER     :: i,j
 !
 ALLOCATE ( aux(ngm),aux2(dfftp%nnr))
 !
 ! read eigenfunctions of Xo
 CALL init_depot( neigvchi0, depotchi0, -1, iuchi0 )
 ! 
 ! loop over neigvchi0
 !
 aux(:) = ZERO
 DO i = 1, neigvchi0
    ! pass depotchi0 to auxilary variable
    aux2(:) = ZERO; aux2(nl(:)) = depotchi0(:,i)
    !if q=(0,0,0), make sure that eigenpotentials of chi0 are real in R-space
    if (lgamma) then
       ! go back to R-space 
       CALL invfft ('Dense', aux2, dfftp)
       ! keep only the real part
       aux2(:) = CMPLX(DBLE(aux2(:)), 0.D0, kind=DP )
       ! go to G-space
       CALL fwfft ('Dense', aux2, dfftp)
    end if
    !
    sum_tmp = ZERO
    DO j = 1, ngm
       sum_tmp = sum_tmp + CONJG(aux2(nl(j)))*dv(j) 
    ENDDO 
#if defined __MPI
        CALL mp_sum( sum_tmp, intra_pool_comm )
#endif 
    zfac = sum_tmp
    !
    ! accumulate over neigvchi0
    !
    aux(:) = aux(:) + aux2(nl(:))*zfac
    !
 ENDDO
    ! (1-sum|Vi><Vi|)dv>
    dv(:) = dv(:) - aux(:)  
    !
 DEALLOCATE ( aux, aux2)
 !
 RETURN
 !
END SUBROUTINE projector
!
!------------------------------------------------------------------
SUBROUTINE vc_dvg( ndim, xq, dvg)
 !
 ! This routine computes results of applying Vc to a potential
 ! associated with vector q. Coulomb cutoff technique can be used
 !
 USE kinds,         ONLY : dp
 USE io_global,     ONLY : stdout
 USE constants,     ONLY : fpi, e2
 USE cell_base,     ONLY : tpiba2, tpiba, omega
 USE gvect,         ONLY : ngm, g, nl, gstart, nlm
 USE gvecs,         ONLY : ngms
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
 INTEGER, INTENT(IN) :: ndim
 ! input: dimension of the arrays for potential and charge density
 REAL(DP), INTENT(IN) :: xq(3)
 ! input: coordinates of q-vector
 COMPLEX(DP), INTENT(INOUT) :: dvg(ndim)
 ! inout: potentials in G-space (will be changed on exit)
 !
 ! local variables
 !
 REAL(DP) :: qg2, fac
 INTEGER  :: ig
 !
 ! various checks
 !
 IF ( ndim .GT. ngm ) CALL errore('vc_dvg', 'ndim too large', 1)
 IF ( ndim .LE. 0   ) CALL errore('vc_dvg', 'ndim negative!', 1)
 !
 DO ig = 1, ngm 
   !
   qg2 = (g(1,ig)+xq(1))**2 + (g(2,ig)+xq(2))**2 + (g(3,ig)+xq(3))**2
   !
   IF(qg2 > 1.d-10) THEN
     fac = (omega*e2*fpi)/(qg2*tpiba2)
     dvg(ig) = dvg(ig) * cmplx(fac, 0.0_DP)    
   ENDIF 
   !
 END DO
 !
 RETURN
 !
END SUBROUTINE vc_dvg
!------------------------------------------------------------------
SUBROUTINE update_pot_rpa( ndim, dnr, dvr)
 !
 ! This routine computes results of applying the approximated full 
 ! response function marix to a density, then obtain potential.
 !
 USE kinds,            ONLY : DP
 USE io_global,        ONLY : stdout
 USE constants,        ONLY : fpi, e2
 USE fft_base,         ONLY : dffts, dfftp
 USE fft_interfaces,   ONLY : fwfft, invfft
 USE cell_base,        ONLY : tpiba2, tpiba
 USE gvect,            ONLY : ngm, g, nl, gstart, nlm
 USE qpoint,           ONLY : xq
 USE control_acfdt,    ONLY : lowest_eigchi0 
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
 INTEGER, INTENT(IN) :: ndim
 ! input: dimension potential and charge density
 !
 REAL(DP), INTENT(IN) :: dnr(ndim)
 ! in   : density in R-space 
 REAL(DP), INTENT(OUT):: dvr(ndim)
 ! out  : potential in R-space
 !
 ! local variables
 !
 INTEGER    :: ig
 REAL(DP)   :: qg, qg2, fac
 REAL(DP), PARAMETER :: const1 = 1.0_DP, const2 = 3.5_DP
 COMPLEX(DP):: sum_tmp
 COMPLEX(DP), ALLOCATABLE :: aux(:),  dng(:)
 REAL(DP), ALLOCATABLE    :: dvr_tmp(:)
 ! density/potential in G-space
 !
 ! various checks
 !
 ALLOCATE ( dvr_tmp(dfftp%nnr), aux(dfftp%nnr), dng(ngm) )
 !
 IF ( ndim .GT. dfftp%nnr) CALL errore('vc_dvg', 'ndim too large', 1)
 IF ( ndim .LE. 0   ) CALL errore('vc_dvg', 'ndim negative!', 1)
 !
 !\sum_i{ |w_i>a_i^-1<w_i|}dn>
 !aux(:) = 0.0_DP; aux(:) = dnr(:)
 CALL apply_inverted_chi0 (dfftp%nnr, dnr, dvr_tmp)
 !
 !put dng on the FFT grid
 aux(:) = ZERO; aux(:) = cmplx(dnr(:),0.0_DP)
 !go to G-space
 CALL fwfft ('Dense', aux, dfftp)
 dng(:) = aux(nl(:))
 !
 !(1-\sum_i{ |w_i><w_i|})*a_max^-1|dn>
 CALL projector(ngm, dng)
 DO ig = 1, ngm
    qg2 = (g(1,ig)+xq(1))**2 + (g(2,ig)+xq(2))**2 + (g(3,ig)+xq(3))**2
    IF (qg2 > 1.d-10) THEN
       !fac = (e2*fpi)/(qg2*tpiba2)
       !fac = -((1+qg2*const1)**2)*(e2*fpi)/(const2*qg2*tpiba2)
       fac = 1.0_DP/lowest_eigchi0
       dng(ig) = dng(ig) *cmplx(fac, 0.0_DP)
    ENDIF
 ENDDO
 !
 !put dng on the FFT grid
 aux(:) = ZERO; aux(nl(:)) = dng(:)
 !go to R-space
 CALL invfft ('Dense', aux, dfftp)
 dvr(:) = 0.0_DP; dvr(:) = dble(aux(:))
 !
 dvr(:) = dvr(:) + dvr_tmp(:) 
 !
 DEALLOCATE ( dvr_tmp, aux, dng)
 !
 RETURN
 !
END SUBROUTINE update_pot_rpa
!------------------------------------------------------------
SUBROUTINE norm_newdden(ndim, dden, ddot)
!-----------------------------------------------------------
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE gvect,         ONLY : ngm, nl
 USE fft_base,      ONLY : dfftp
 USE fft_interfaces,ONLY : fwfft, invfft

 IMPLICIT NONE
 !
 ! I/O variables
 !
 INTEGER, INTENT(IN)     :: ndim  
 REAL(DP), INTENT(INOUT) :: dden(ndim)
 REAL(DP), INTENT(INOUT) :: ddot
 !
 ! Local variables 
 !
 INTEGER :: i 
 COMPLEX(DP), ALLOCATABLE :: aux(:)
 ! auxiliary arrays for FFT
 REAL(DP):: sum_tmp, norm
 !
 ALLOCATE(aux(ngm))
 !
 aux(:) = ZERO; aux(:) = dden(:)
 !
 sum_tmp = ZERO
 DO i = 1, ngm
 sum_tmp = sum_tmp + CONJG(aux(i))*aux(i)
 ENDDO
#if defined __MPI 
 CALL mp_sum( sum_tmp, intra_pool_comm )
#endif  
 !
 ddot = DSQRT(DBLE(sum_tmp))
 !
 norm = 1.0_DP/DSQRT(DBLE(sum_tmp))
 !
 aux(:) = norm*aux(:)
 !
 dden(:) = ZERO; dden(:) = aux(:)
 !
 DEALLOCATE (aux)
 !
 RETURN
 !
END SUBROUTINE norm_newdden

                                      


