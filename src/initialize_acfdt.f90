!
! Copyright (C) 2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
SUBROUTINE initialize_acfdt()
  !-----------------------------------------------------------------------
  !
  ! This is a driver to the phonon initialization routines.
  !
  USE klist,      ONLY : nks
  USE qpoint,     ONLY : nksq, ikks, ikqs
  USE control_lr, ONLY : lgamma
  USE control_ph, ONLY : all_done
  USE acfdtest,   ONLY : skip_ph 
  !
  IMPLICIT NONE
  INTEGER :: ik
  !
  ! ... nksq is the number of k-points, NOT including k+q points
  !
  IF ( lgamma ) THEN
     !
     nksq = nks
     ALLOCATE(ikks(nksq), ikqs(nksq))
     DO ik=1,nksq
        ikks(ik) = ik
        ikqs(ik) = ik
     ENDDO
     !
  ELSE
     !
     nksq = nks / 2
     ALLOCATE(ikks(nksq), ikqs(nksq))
     DO ik=1,nksq
        ikks(ik) = 2 * ik - 1
        ikqs(ik) = 2 * ik
     ENDDO
     !
  END IF
  !
  !  Allocate the phonon variables
  !
  CALL allocate_phq()
  !
  !  Set the main control variable of the phonon code
  !
  skip_ph =.TRUE. 
  CALL phq_setup_1()
  skip_ph =.FALSE. 
  !
  !  Recover the status if available
  !
  all_done =.FALSE. 
  !
  !  Open the files of the phonon code
  !
  CALL openfilq()
  !
  !  Initialize all quantities which do not depend on the 
  !  linear response to the perturbation
  !
  CALL phq_init()
  !
  CALL print_clock( 'NSCF' )
  !
  RETURN
  
END SUBROUTINE initialize_acfdt
!
!----------------------------------------------------------------------
subroutine allocate_acfdt ( )
  !--------------------------------------------------------------------
  !
  !	This subroutine allocates and initializes global variables used 
  !     in acdft program 
  !
  USE kinds,         ONLY : DP
  USE gvect,         ONLY : ngm, ngm_g
  USE fft_base,      ONLY : dfftp
  USE lsda_mod,      ONLY : nspin
  USE wvfct,         ONLY : npwx, nbnd
  USE control_lr,    ONLY : lgamma
  USE control_acfdt, ONLY : tu_ec, u_ec, wu_ec, dvgen, dvgenc, eigchi0, depotchi0, &
                            depot, dvgg, fockevc, exxvx, save_exx, rpavc, sum_dchi, &
                            sum_dchi3, sum_dchi12, dchi_iu, dchi_iu3, dchi_iu12, &
                            save_rpa, save_rpa12, save_rho, vc_init, vx_init, &
                            vx_exx, vc_rpa, nf_ec, neigv, neigvchi0, delta_vxc, &
                            delta_vxc_old, save_vloc, approx_chi0, chi0_is_solved, &
                            inverted_chi, pot_collect
  !
  implicit none
  !
  chi0_is_solved = .FALSE.
  inverted_chi   =  approx_chi0  
  !
  allocate( tu_ec(nf_ec), u_ec(nf_ec), wu_ec(nf_ec) )
  !
  if (lgamma) then
     allocate (dvgen  (dfftp%nnr, nspin))
     allocate (dvgenc (dfftp%nnr, nspin, 2))
  else
     allocate (dvgenc (dfftp%nnr, nspin, 2))
  endif
  !
  ! variables for ec_rpa
  allocate ( eigchi0(neigvchi0) )  
  allocate ( depotchi0(ngm,neigvchi0) )
  allocate ( depot(ngm,neigv) )
  if ( pot_collect ) allocate ( dvgg(ngm_g) )
  !
  ! variables for vexx or vc_rpa
  if (vx_exx) then
     allocate ( fockevc (npwx,nbnd), exxvx(dfftp%nnr) )
     exxvx(:) = 0.0_DP
  end if
  !
  ! variable for vc_rpa
  if (vc_rpa)  then
     allocate (rpavc(dfftp%nnr), sum_dchi(dfftp%nnr), dchi_iu ( dfftp%nnr))
     allocate (sum_dchi3(dfftp%nnr), dchi_iu3 (dfftp%nnr))
     allocate (sum_dchi12(dfftp%nnr), dchi_iu12 (dfftp%nnr))
     rpavc(:) = 0.0_DP
  endif
  !
  if (vx_exx .or. vc_rpa) then
     allocate ( vc_init (dfftp%nnr,nspin), vx_init(dfftp%nnr,nspin), delta_vxc(dfftp%nnr), delta_vxc_old(dfftp%nnr) )
     vx_init(:,:) = 0.0_DP
     vc_init(:,:) = 0.0_DP
     delta_vxc(:) = 0.0_DP
     delta_vxc_old(:) = 0.0_DP
  end if
  !
  ! variables for oep proceduce
  if (vx_exx) then
     allocate ( save_exx(dfftp%nnr))
  endif
  if (vc_rpa) then
     allocate ( save_rpa (dfftp%nnr))
     allocate ( save_rpa12(dfftp%nnr))
  endif
  allocate ( save_rho (dfftp%nnr))
  allocate ( save_vloc(dfftp%nnr,nspin))
  save_vloc(:,:) = 0.0_DP
  !
  return
  
end subroutine allocate_acfdt
!
!----------------------------------------------------------------------
subroutine deallocate_acfdt ( )
  !--------------------------------------------------------------------
  !
  !	This subroutine allocates and initializes global variables used 
  !     in acdft program 
  !
  USE control_acfdt, ONLY : tu_ec, u_ec, wu_ec, dvgen, dvgenc, eigchi0, depotchi0, &
                            depot, dvgg, fockevc, exxvx, save_exx, rpavc, sum_dchi, &
                            sum_dchi3, sum_dchi12, dchi_iu, dchi_iu3, dchi_iu12, &
                            save_rpa, save_rpa12, save_rho, vc_init, vx_init 
  !
  implicit none
  !
  if ( allocated(tu_ec) ) deallocate(tu_ec) 
  if ( allocated(u_ec)  ) deallocate(u_ec) 
  if ( allocated(wu_ec) ) deallocate(wu_ec) 
  !
  if ( allocated(dvgen) )  deallocate(dvgen )
  if ( allocated(dvgenc))  deallocate(dvgenc)
  !
  ! variables for ec_rpa
  if ( allocated(eigchi0)     ) deallocate(eigchi0    )  
  if ( allocated(depotchi0)   ) deallocate(depotchi0  )  
  if ( allocated(depot)       ) deallocate(depot      )  
  if ( allocated(dvgg)        ) deallocate(dvgg       )  
  !
  ! variable for vx_exx
  if ( allocated(fockevc)) deallocate(fockevc)  
  if ( allocated(exxvx)  ) deallocate(exxvx  )  
  if ( allocated(save_exx)  ) deallocate(save_exx  )
  !
  ! variable for vc_rpa
  if ( allocated(rpavc)   ) deallocate(rpavc )   
  if ( allocated(sum_dchi)) deallocate(sum_dchi )  
  if ( allocated(sum_dchi)) deallocate(sum_dchi3)  
  if ( allocated(sum_dchi)) deallocate(sum_dchi12)  
  if ( allocated(dchi_iu )) deallocate(dchi_iu )
  if ( allocated(dchi_iu )) deallocate(dchi_iu3)
  if ( allocated(dchi_iu )) deallocate(dchi_iu12)
  if ( allocated(save_rpa)) deallocate(save_rpa)  
  if ( allocated(save_rpa12)) deallocate(save_rpa12)  
  !
  if ( allocated(save_rho)) deallocate(save_rho)
  if ( allocated(vc_init) ) deallocate(vc_init )
  if ( allocated(vx_init) ) deallocate(vx_init )
  
  return
  !
end subroutine deallocate_acfdt
