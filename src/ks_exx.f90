!
! Copyright (C) 2001-2004 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!----------------------------------------------------------------------------
SUBROUTINE ks_exx ( )
  !----------------------------------------------------------------------------
  !
  !    This routine computes exact-exchange energy from given KS orbitals.
  !
  USE kinds,                ONLY : DP
  USE io_global,            ONLY : stdout
  USE klist,                ONLY : xk, nks, nkstot, ngk, igk_k
  USE noncollin_module,     ONLY : npol
  USE buffers,              ONLY : open_buffer, get_buffer, close_buffer
  USE wavefunctions_module, ONLY : evc
  USE io_files,             ONLY : prefix, iunwfc, nwordwfc
  USE wvfct,                ONLY : npwx,nbnd
  USE gvect,                ONLY : ngm, g
  USE gvecw,                ONLY : gcutw
  USE funct,                ONLY : init_dft_exxrpa, dft_is_hybrid
  USE exx,                  ONLY : exx_grid_init, exx_div_check, exxinit, &
                                   exxenergy, exxenergy2, exx_mp_init

  !
  IMPLICIT NONE
  !
  REAL(DP), ALLOCATABLE :: gk(:)
  INTEGER :: ik, npw
  LOGICAL :: exst
  !
  ! NsC Do we need it after igk_k is kept in memory?? 
  ! setup for npw, igk, ...
  ! 
  nks = nkstot
  !
!  REWIND( iunigk )
  !
  !
  ALLOCATE ( gk(npwx) )
  igk_k(:,:) = 0
  DO ik = 1, nks
     !
     ! ... g2kin is used here as work space
     !
     CALL gk_sort( xk(1,ik), ngm, g, gcutw, ngk(ik), igk_k(1,ik), gk )
     !
     ! ... if there is only one k-point npw and igk stay in memory
     !
     npw = ngk(ik)
!     igk(:) = igk_k(:,ik)
!     IF ( nks > 1 ) WRITE( iunigk ) npw, igk
     !
  END DO
  DEALLOCATE ( gk )
  !
  ! wfc stuffs
  !
  iunwfc = 20; nwordwfc = nbnd * npwx * npol
  CALL open_buffer( iunwfc, 'wfc', nwordwfc, nks, exst )
  IF (.NOT.exst) THEN
     CALL errore ('openfilq', 'file '//trim(prefix)//'.wfc not found', 1)
  END IF
  ! if only 1 k-point, wfc is read here
  CALL get_buffer (evc, nwordwfc, iunwfc, 1)
  !
  ! initialization for exact-exchange energy calculation
  !
  !WRITE (stdout,*) "exx 1: dft_is_hybrid = ",dft_is_hybrid(),exx_is_active()
  IF ( .NOT. dft_is_hybrid() ) CALL init_dft_exxrpa ()
  !
  IF ( dft_is_hybrid() ) THEN
     CALL exx_grid_init()
     CALL exx_mp_init()
     CALL exx_div_check()
  ENDIF
  !WRITE (stdout,*) "exx 2: dft_is_hybrid = ",dft_is_hybrid(), exx_is_active()
  !
  call exxinit ()
  !
  write(stdout,9064) 0.5d0*exxenergy(), 0.5d0*exxenergy2()                   
  !
9064 format( '     + Fock energy             =',F17.8,' Ry = ', F17.8,' Ry ',/)   

  !
  CALL close_buffer( iunwfc, 'KEEP' )
  !
  RETURN
  !
END SUBROUTINE ks_exx
