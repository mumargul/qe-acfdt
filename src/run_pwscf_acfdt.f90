!
! Copyright (C) 2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
SUBROUTINE run_pwscf_acfdt (iter)
  !-----------------------------------------------------------------------
  !
  ! ... This is the main driver of the pwscf program called from the
  ! ... phonon code.
  !
  !
  USE kinds,           ONLY : DP
  USE control_flags,   ONLY : lscf, niter, tr2, mixing_beta, nmix, twfcollect, david
  USE basis,           ONLY : starting_pot, starting_wfc, startingconfig
  USE lsda_mod,        ONLY : nspin, current_spin
  USE funct,           ONLY : enforce_dft_exxrpa
  USE mp_global,       ONLY : npot => nimage
  USE control_acfdt,   ONLY : potcorr
  USE acfdt_scf,       ONLY : dv_update, print_save_result, print_result
  USE scf,             ONLY : vrs
  USE acfdtest,        ONLY : off_vrs_setup 
  USE xml_io_base,     ONLY : write_rho
  USE io_files,        ONLY : tmp_dir, prefix
  !
  IMPLICIT NONE
  !
  INTEGER, INTENT(IN) :: iter
  !
  CHARACTER(LEN=256) :: dirname, file_base_in, file_base_out
  LOGICAL :: exst
  dirname = TRIM(tmp_dir) // TRIM(prefix) // '.save/'
  !
  ! ... Setting the values for the pw-nonscf run
  !
  CALL start_clock( 'PWSCF' )
  !
  ! ... initialize variables needed in electronic self-consistency
  !
  current_spin = 1 
  IF( nspin /= 1 ) &
    CALL errore( 'setup', 'nspin should be 1; check iosys', 1 )
  !
  lscf = .TRUE.
  niter = 50; tr2 = 5.D-12; mixing_beta = 0.7d0; nmix = 8
  david = 4; lscf  = .FALSE.
  !
  starting_pot = 'file'
  starting_wfc = 'file'
  startingconfig = 'input'
  !
  IF(.NOT.potcorr) CALL enforce_dft_exxrpa( )
  !
  CALL setup () 
  !
  CALL init_run()
  !
  ! ... setup the local potential 
  !
  vrs(:,:) = 0.0_DP 
  vrs(:,:) = dv_update(:,:) 
  !
  ! NsC ACFDT: We just need a non_scf calculation (lscf=.FALSE.). non_scf subroutine was called inside electrons
  !            before svn version 10107 but no more for later version. Not sure if the initialization part of
  !            electrons subroutine is needed for ACFDT stuff but it seems not. A call to non_scf should be enough.
  !CALL electrons()
  CALL non_scf ()
  !
  !IF (.NOT.reduce_io) THEN
     !
  twfcollect=.FALSE.
  IF (npot > 1) twfcollect=.TRUE.
  CALL punch( 'all' )
     !
  ! ENDIF
  !
  ! save again the density and potential
  !
  CALL write_rho(dirname, dv_update, nspin, 'potential_KS_effective_pot')
  !
  ! ... setup the control flags for printing, and saving results
  ! ... in next scf steps. 
  !
  ! 1) deactivate setingup vrs(:,:) in PH/phq_setup.f90 in next steps.
  !    This potential is used as the optimized one computed in acfdt_estimate_veff_kspot.f90 
  !  
  off_vrs_setup = .true.
  !
  ! 2) in case of quadratic or cubic line seach methods used, if vrs is computed from optimized
  !    alpha, the density at this time is optimized one, and so does energies computed begining 
  !    in the next scf step. We activate printing and saving energies, potentials, and density.
  !
  print_save_result = print_result
  !
  !CALL seqopn( 4, 'restart', 'UNFORMATTED', exst )
  !CLOSE( UNIT = 4, STATUS = 'DELETE' )
  !ext_restart=.FALSE.
  !
  CALL close_files(.false.)
  !
  CALL stop_clock( 'PWSCF' )
  !
  RETURN
  !
END SUBROUTINE run_pwscf_acfdt

SUBROUTINE print_acfdt_total_energy  (iter)
  !
  USE kinds,           ONLY : DP
  USE io_global,       ONLY : stdout
  USE fft_base,        ONLY : dfftp
  USE lsda_mod,        ONLY : nspin
  USE acfdt_ener,      ONLY : acfdt_eband
  USE scf,             ONLY : rho
  USE control_acfdt,   ONLY : ex_exx, ecrpa, ec_sr,   &
                              vx_exx, vc_rpa 
  USE acfdt_scf,       ONLY : etot_old, print_save_result, & 
                              iter_oep, iter_save,         &
                              scf_rpa_plus, rho_1step
  !
  IMPLICIT NONE
  !
  INTEGER, INTENT (INOUT) :: iter
  !
  ! local variables
  !
  REAL(DP) :: etot
  !
  etot = acfdt_eband
  !
  ! ... print energies ...
  !
  IF(vx_exx) etot = etot + ex_exx
  IF(vc_rpa) etot = etot + ecrpa
  !
  ! ... strict treament for first scf step
  !
  IF (iter==1) THEN
     !  
     print_save_result = .TRUE.
     !
     ALLOCATE (rho_1step (dfftp%nnr, nspin))
     rho_1step(:,:) = rho%of_r(:,:) 
     !
     ! rho_1step will be deallocated in stop_acfdt_scf_exxrpa   
     !
  ENDIF
  !
  !
  IF (.not. print_save_result) THEN
     !
     WRITE(stdout,9009) iter
     WRITE(stdout,9083) etot
     WRITE(stdout,9060) acfdt_eband, ex_exx + ecrpa
     WRITE(stdout,9061) ex_exx
     WRITE(stdout,9062) ecrpa
     !
  ENDIF
  !
  IF (print_save_result) THEN
     !
     WRITE(stdout,9010) iter_oep
     WRITE(stdout,9081) etot
     WRITE(stdout,9060) acfdt_eband, ex_exx + ecrpa
     WRITE(stdout,9061) ex_exx
     WRITE(stdout,9062) ecrpa
     !
     IF (scf_rpa_plus) THEN
        !
        WRITE(stdout,9063) ec_sr
        etot = etot + ec_sr
        WRITE(stdout,9082) etot
        !
     ENDIF
     !
     iter_oep = iter_oep + 1
     !
  ENDIF
  !
  ! update scf step_th index
  !
  iter_save = iter 
  !
  RETURN 
  !
  ! ... formats
  !
  9009 FORMAT(/'     SCF iteration #',I3)
  9010 FORMAT(/'     SCF steps #',I3)
  9060 FORMAT(/'     The total energy is the sum of the following terms:',/,&
              /'     one-electron contribution =',F17.8,' Ry' &
              /'     xc contribution           =',F17.8,' Ry' )
  9061 FORMAT( '     exact exchange energy     =',F17.8,' Ry' )
  9062 FORMAT( '     rpa correlation energy    =',F17.8,' Ry' )
  9063 FORMAT( '     short-range energy        =',F17.8,' Ry' )
  9081 FORMAT(/'     minimum_total energy      =',0PF17.8,' Ry' )
  9083 FORMAT(/'     total energy             =',0PF17.8,' Ry' )
  9082 FORMAT(/'!    short-range corrected total energy      =',0PF17.8,' Ry' )
  !
END SUBROUTINE print_acfdt_total_energy 
!
!--------------------------------------------------------------------
SUBROUTINE stop_acfdt_scf_exxrpa ( conv, iter)
  !------------------------------------------------------------------
  !
  ! This routine checks the convergency of OEP procedure
  !            |n_i(r) - n_i-1(r)| <=10^-6
  !
  USE kinds,           ONLY : DP
  USE io_global,       ONLY : stdout, meta_ionode_id
  USE fft_base,        ONLY : dfftp
  USE cell_base,       ONLY : omega
  USE lsda_mod,        ONLY : lsda, nspin
  USE xml_io_base,      ONLY : read_rho, write_rho
  USE control_acfdt,   ONLY : thr_oep
  USE acfdt_scf,       ONLY : print_save_result, rho_1step 
  USE scf,             ONLY : rho
  USE mp_global,       ONLY : intra_pool_comm, inter_image_comm, my_pot_id => my_image_id, world_comm, &
                              inter_pot_comm => inter_image_comm, npot => nimage
  USE mp,              ONLY : mp_sum, mp_bcast
  !
  USE io_files,      ONLY : tmp_dir, prefix
  !
  IMPLICIT NONE
  !
  LOGICAL, INTENT (INOUT) :: conv
  INTEGER, INTENT (INOUT) :: iter
  !
  ! local variable
  !
  INTEGER  :: is
  REAL(DP) :: sum_tmp
  !
  ! auxiliary variable
  REAL(DP) :: aux_rho(dfftp%nnr, nspin)
  !
  CHARACTER(LEN=256) :: dirname
  dirname = TRIM(tmp_dir) // TRIM(prefix) // '.save/'
  !
  IF (iter == 1) THEN
     ! 
     ! rho_1step was allocated in print_acfdt_total_energy
     !
     !CALL write_rho(dirname, rho_1step, nspin, 'save_rho')
     !
     ! NsC >>> Only the procs in the first image write in prefix.save
     ! CALL write_rho(dirname, rho_1step, nspin)
     IF (my_pot_id == 0) CALL write_rho(dirname, rho_1step, nspin) ! NsC 
     ! NsC <<<
     !
     deallocate (rho_1step)
     !
     conv = .false.
     ! 
     RETURN
     !
  ENDIF
  !
  ! read again the density has been saved 
  !
  aux_rho(:,:) = 0.0_DP
  ! NsC >>> Only procs in the image 0 read the density from prefix.save/charge_density
  IF (my_pot_id == 0 ) CALL read_rho(dirname, aux_rho, nspin )
#if defined __MPI
  IF (npot > 0 ) CALL mp_sum( aux_rho, inter_pot_comm )  ! mp_sum used to broadcast aux_rho to all the pot_images
#endif
  ! NsC <<<
  !
  sum_tmp = 0.0_DP 
  !
  DO is = 1, nspin 
    sum_tmp = sum_tmp + omega * sum((aux_rho(:,is) - rho%of_r(:,is))**2) &
                   / (dfftp%nr1 * dfftp%nr2 * dfftp%nr3)
  ENDDO
  !
#if defined __MPI
  CALL mp_sum( sum_tmp, intra_pool_comm )  
#endif
  !
  sum_tmp = DSQRT(sum_tmp)
  !
  conv = (sum_tmp < 0.1_DP*thr_oep)
  !
  write(stdout,*) "oep convergency: ", sum_tmp, conv
  !
  !IF (print_save_result) CALL write_rho(dirname, rho%of_r, nspin, 'save_rho' )
  ! NsC >>> Only the procs in the root image write the density in prefix.save
  !IF (print_save_result) CALL write_rho(dirname, rho, nspin)
  IF (print_save_result .AND. my_pot_id == 0) CALL write_rho(dirname, rho%of_r, nspin)  ! NsC 
  ! 
#if defined __MPI
  CALL mp_bcast (conv, meta_ionode_id, world_comm)
#endif
  ! NsC <<<
  !
  RETURN
  !
END SUBROUTINE stop_acfdt_scf_exxrpa
