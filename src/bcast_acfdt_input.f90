!
! Copyright (C) 2001-2008 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
subroutine bcast_acfdt_input ( )
  !-----------------------------------------------------------------------
  !
  !     In this routine the first processor sends the acfdt input to all
  !     the other processors
  !
  !
#if defined __MPI

  USE run_info,      ONLY : title
  USE io_global,     ONLY : meta_ionode_id
  USE mp,            ONLY : mp_bcast
  USE mp_global,     ONLY : world_comm
  USE control_ph,    ONLY : nmix_ph, niter_ph, alpha_mix, tr2_ph, recover, ldisp 
  USE control_flags, ONLY : lecrpa
  USE qpoint,        ONLY : xq
  USE disp,          ONLY : nq1, nq2, nq3
  USE partial,       ONLY : nat_todo
  USE io_files,      ONLY : tmp_dir, prefix
  USE control_acfdt, ONLY : approx_chi0, c_mix, depdir1, depdir2, ecut_aux, &
                            idiag, iumax, max_oep_space, neigv, neigvchi0, nf_ec, &
                            nlambda, oep_maxstep, pot_collect, potcorr, rpax, savealldep, &
                            tau1, tau2, thr_ec, thr_oep, u0_ec, vc_rpa, vx_exx, wq
  USE acfdtest,      ONLY : test_oep, acfdt_num_der, ir_point, delta_vrs, f1,f2,f3, & 
                            acfdt_is_active, acfdt_term1, acfdt_term2, acfdt_term3
  USE acfdt_scf,     ONLY : oep_recover, input_lambda, oep_method, stopped_first_iter, &
                            do_acfdt, print_xc_pots, scf_rpa_plus, line_search_index
  !
  implicit none
  !
  call mp_bcast ( title,       meta_ionode_id, world_comm )
  call mp_bcast ( tmp_dir,     meta_ionode_id, world_comm )
  call mp_bcast ( prefix,      meta_ionode_id, world_comm )
  call mp_bcast ( recover,     meta_ionode_id, world_comm )
  !
  call mp_bcast ( lecrpa,      meta_ionode_id, world_comm ) 
  call mp_bcast ( nf_ec,       meta_ionode_id, world_comm ) 
  call mp_bcast ( u0_ec,       meta_ionode_id, world_comm )
  call mp_bcast ( neigv,       meta_ionode_id, world_comm )
  call mp_bcast ( neigvchi0,   meta_ionode_id, world_comm )
  call mp_bcast ( iumax,       meta_ionode_id, world_comm )
  call mp_bcast ( thr_ec,      meta_ionode_id, world_comm )
  call mp_bcast ( idiag,       meta_ionode_id, world_comm )
  call mp_bcast ( pot_collect, meta_ionode_id, world_comm )
  call mp_bcast ( savealldep,  meta_ionode_id, world_comm )
  call mp_bcast ( depdir1,     meta_ionode_id, world_comm )
  call mp_bcast ( depdir2,     meta_ionode_id, world_comm )
  call mp_bcast ( tau1,        meta_ionode_id, world_comm )
  call mp_bcast ( tau2,        meta_ionode_id, world_comm )
  call mp_bcast ( vc_rpa,      meta_ionode_id, world_comm )
  call mp_bcast ( vx_exx,      meta_ionode_id, world_comm )
  !
  call mp_bcast ( tr2_ph,      meta_ionode_id, world_comm )
  call mp_bcast ( niter_ph,    meta_ionode_id, world_comm )
  call mp_bcast ( alpha_mix,   meta_ionode_id, world_comm )
  call mp_bcast ( nmix_ph,     meta_ionode_id, world_comm )
  call mp_bcast ( xq,          meta_ionode_id, world_comm )
  call mp_bcast ( wq,          meta_ionode_id, world_comm )
  call mp_bcast ( ldisp,       meta_ionode_id, world_comm )
  call mp_bcast ( nq1,         meta_ionode_id, world_comm )
  call mp_bcast ( nq2,         meta_ionode_id, world_comm )
  call mp_bcast ( nq3,         meta_ionode_id, world_comm )
  call mp_bcast ( potcorr,     meta_ionode_id, world_comm )
  call mp_bcast ( c_mix,       meta_ionode_id, world_comm )
  call mp_bcast ( oep_maxstep, meta_ionode_id, world_comm )
  call mp_bcast ( thr_oep    , meta_ionode_id, world_comm )
  call mp_bcast ( max_oep_space, meta_ionode_id, world_comm )
  call mp_bcast ( ecut_aux,    meta_ionode_id, world_comm )
  call mp_bcast ( oep_method,    meta_ionode_id, world_comm )
  !
  call mp_bcast ( acfdt_is_active, meta_ionode_id, world_comm )
  call mp_bcast ( acfdt_num_der  , meta_ionode_id, world_comm )
  call mp_bcast ( oep_recover    , meta_ionode_id, world_comm )
  call mp_bcast ( approx_chi0    , meta_ionode_id, world_comm )
  call mp_bcast ( input_lambda   , meta_ionode_id, world_comm )
  call mp_bcast ( stopped_first_iter,    meta_ionode_id, world_comm )
  !
  call mp_bcast ( ir_point ,     meta_ionode_id, world_comm )
  call mp_bcast ( delta_vrs,     meta_ionode_id, world_comm )
  call mp_bcast ( f1,            meta_ionode_id, world_comm )
  call mp_bcast ( f2,            meta_ionode_id, world_comm )
  call mp_bcast ( f3,            meta_ionode_id, world_comm )
  call mp_bcast ( acfdt_term1, meta_ionode_id, world_comm )
  call mp_bcast ( acfdt_term2, meta_ionode_id, world_comm )
  call mp_bcast ( acfdt_term3, meta_ionode_id, world_comm )
  call mp_bcast ( test_oep   , meta_ionode_id, world_comm )
  call mp_bcast ( do_acfdt   , meta_ionode_id, world_comm )
  call mp_bcast ( print_xc_pots, meta_ionode_id, world_comm )
  call mp_bcast ( scf_rpa_plus, meta_ionode_id, world_comm )
  call mp_bcast ( line_search_index, meta_ionode_id, world_comm )
  !
  call mp_bcast ( rpax  , meta_ionode_id, world_comm )
  call mp_bcast ( nlambda  , meta_ionode_id, world_comm )
  !
#endif
  !
  return
  !
end subroutine bcast_acfdt_input
