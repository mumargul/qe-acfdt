!
! Copyright (C) 2001-2011 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-------------------------------------------------------------------
  SUBROUTINE acfdt_scf_exxrpa
    !-----------------------------------------------------------------
    !
    ! ... This is the driver of OEP within ACFDT theorem.
    ! ... It reads all the quantities calculated by pwscf, it
    ! ... checks if some recover file is present and determines
    !
    USE io_global,       ONLY : stdout
    USE mp_global,       ONLY : my_pot_id=>my_image_id
    USE disp,            ONLY : nqs
    USE control_lr,      ONLY : lgamma
    USE control_flags,   ONLY : lscf, twfcollect
    USE control_acfdt,   ONLY : efieldper, vx_exx, vc_rpa, oep_maxstep, inverted_chi
    USE acfdt_scf,       ONLY : do_acfdt
    !
    IMPLICIT NONE
    !
    INTEGER   :: max_iter                     ! maximum OEP iteration 
    INTEGER   :: iq, inum, num_iter
    LOGICAL   :: do_band, do_iq, setup_pw, conv
    CHARACTER (LEN=9)   :: code = 'ACFDT'
    CHARACTER (LEN=256) :: auxdyn
    !
    ! ... Check if calculation is restarted or not ...
    !
    CALL check_initial_oep_status (num_iter)
    ! 
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!! iterate !!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    max_iter = oep_maxstep
    !
    DO inum = 1, max_iter  ! start scf loop
       !
       num_iter = num_iter + 1
       !
       IF ( vx_exx .OR. vc_rpa ) THEN
          WRITE(stdout,'(/,/,"ITER #",i3," : LOOP OVER EXXRPA POT",/,/)') num_iter
       ENDIF
       !
       ! Here we compute EXX/RPA from scf density within ACFD theory.
       ! Local multiplicative exact exchange and RPA correlation potentials
       ! are calculated 
       !
       DO iq = 1, nqs ! start loop over q points 
          !
          CALL setup_q(auxdyn, do_band, do_iq, setup_pw, iq)
          !CALL prepare_q(auxdyn, do_band, do_iq, setup_pw, iq)
          !
          ! If this q is not done in this run, cycle
          !
          IF (.NOT.do_iq) CYCLE
          !
          ! If necessary the bands are recalculated
          ! lscf=.FALSE. by default (declared in Modules/control_flags.f90),
          ! so one-shot RPA should be ok. But if the loop over exxrpa potential
          ! is peformed, lscf must be reset to .FALSE. at each iteration
          lscf = .FALSE.
          !  If necessary the bands are recalculated
          !
          IF (setup_pw) CALL run_pwscf(do_band, iq)
          !
          !  Initialize the quantities which do not depend on
          !  the linear response of the system
          !
          CALL initialize_acfdt()
          !
          !
          IF (lgamma) THEN    
             ! 
             ! If exx/rpa potential is calculated, we compute eigenfunctions
             ! of Xo here. But first vrs must be reset since it is recalculated
             ! in initialize_acfdt from density without xc contribution
             !
             IF ( vx_exx .OR. vc_rpa ) THEN
                IF(inverted_chi) CALL chi0_eigfct () 
             ENDIF
             ! 
          ENDIF 
          !
          ! compute exact exchange potential if required
          ! NB: if q=/0, xk(:) also contains k+q, nks will be doubled.
          ! thus exxinit in exx.f90 does not work properly.
          ! need to think more about how to overcome this problem!
          !
          ! compute RPA Ec and possibly the potential
          !
          efieldper = .false.
          IF (do_acfdt) CALL ec_acfdt (num_iter, vc_rpa )
          !
          ! compute exact exchange and rpa potential if required
          ! NB: since the potential is real and periodic, it is done
          ! only at q=/0, xk(:), calculation at q\= point is done after
          ! other finite q  
          ! 
          IF (lgamma) THEN
             !
             ! compute exx
             !  
             CALL callexx ( num_iter, vx_exx ) 
             !
             ! ... print out minimum total energy
             !
             CALL print_acfdt_total_energy (num_iter)
             !
             ! compute vxxrpa
             ! 
             CALL acfdt_estimate_veff_kspot (num_iter) 
             ! 
          ENDIF
          !
          ! ... print out minimum total energy
          !
          !CALL print_acfdt_total_energy (num_iter)
          !
          ! ... cleanup of the variables for the next q point
          !
          !  NsC: compatibility with QE-10046
          twfcollect =.FALSE.   
          !
          CALL clean_pw_ph(iq)
          !
       ENDDO ! end loop over q_points 
       !
       ! ... electronic self-consistency with fixed Vx-exx and Vc-rpa
       !
       IF ( vx_exx .OR. vc_rpa ) THEN
          CALL run_pwscf_acfdt ( num_iter ) 
       END IF
       !
       ! ... check stop pwscf.... |n_i(r) - n_i-1(r)| <=10^-6
       !
       CALL stop_acfdt_scf_exxrpa (conv, num_iter)
       !  
       ! NsC >>> Only procs in root_pot write on prefix.save
       !CALL write_oep_status( )
       IF (my_pot_id == 0) CALL write_oep_status( )
       ! NsC <<<
       ! 
       IF(conv) EXIT 
       !
    ENDDO ! end scf loop
    !
    RETURN 
    !
  END SUBROUTINE acfdt_scf_exxrpa
