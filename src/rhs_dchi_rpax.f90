!
! Copyright (C) 2008 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
SUBROUTINE rhs_dchi_rpax (ikk, ikq, evc, evq, dpsi_plus, dpsi_minus, dpsi_nl, dpsiq_nl, dv, dv2)
!------------------------------------------------------------------------
  !
  ! This routine to calculate rhs of linear eqs of |psi_2>.
  ! |dvdpsi_v> = |dvdpsi_v> - S|dpsi_v'><psi_v'|(DV)*|psi_v>  (1)
  ! This routine will do: 
  !        1) ps = <psi_v'|dvpsi_v>
  !        2) calc. S|dpsi_v'>
  !        3) apply (1) to find |dvdpsi>
  ! In general, this routine works like routine orthogonalize ()
  !
  ! NB: INPUT is dvpsi and dpsi ; 
  !   : dpsi_1 is used as work_space of S|dpsi> calculation
  !   : OUTPUT is dvdpsi
  !
  USE kinds,                 ONLY : DP
  USE cell_base,             ONLY : omega
  USE lsda_mod,              ONLY : lsda, isk, current_spin, nspin
  USE klist,                 ONLY : xk, ngk, igk_k
  USE noncollin_module,      ONLY : noncolin, npol
  USE wvfct,                 ONLY : npwx, nbnd
  USE gvect,                 ONLY : g, ngm
  USE gvecs,                 ONLY : nls, doublegrid
  USE qpoint,                ONLY : nksq, ikks, ikqs
  USE control_lr,            ONLY : nbnd_occ
  USE mp_global,             ONLY : intra_pool_comm
  USE mp,                    ONLY : mp_sum
  USE units_ph,              ONLY : iudwf, iuwfc, lrdwf, lrwfc
  USE fft_base,              ONLY : dffts, dfftp
  USE fft_interfaces,        ONLY : fwfft, invfft
  USE control_acfdt,         ONLY : exxvx
  USE exx,                   ONLY : vexx, g2_convolution

!
 IMPLICIT NONE
 COMPLEX(DP), ALLOCATABLE :: dpsi_1(:,:,:) ! work space allocated by the calling routine

 integer, parameter :: npe = 1            
 INTEGER, INTENT(IN) :: ikk ,ikq
 INTEGER :: npw,npwq
 COMPLEX(DP), INTENT(IN) :: evc(npwx,nbnd), evq(npwx,nbnd)
 COMPLEX(DP), INTENT(IN) :: dpsi_plus(npwx,nbnd), dpsi_minus(npwx,nbnd)
 COMPLEX(DP), INTENT(IN) ::dpsi_nl(npwx,nbnd), dpsiq_nl(npwx,nbnd)
 ! input: first order variation of wfc wrt dynamical potential dv for +/- iu and [Vx-vx]
 COMPLEX(DP), INTENT(INOUT) :: dv2(npwx,nbnd,2)
 ! inout: rhs of linear system for dpsi2
 COMPLEX(DP), INTENT(IN) :: dv(dffts%nnr , nspin , npe)
 ! input: dynamical potential
 integer ::  ig,         & ! counter on G vectors
             ir,         & ! counter on mesh points
             is,         & ! counter on spin polarizations
             ip,         & ! counter kpoint
             ios,        &
             ipp,        & ! index of kpoint p
             ipq           ! index of kpoint p+q


 COMPLEX(DP), ALLOCATABLE :: aux1(:), aux2(:), psi_r(:), phi_r(:), dpsi_p_r(:), dpsi_m_r(:)
 COMPLEX(DP), ALLOCATABLE :: result1(:), result2(:), rhoc(:), rhoc_p(:),rhoc_m(:)
 COMPLEX(DP), ALLOCATABLE :: vc(:), vc_p(:), vc_m(:)
 REAL(DP) , ALLOCATABLE :: fac(:)
 COMPLEX(DP), ALLOCATABLE :: phi(:,:) , dpsi_p(:,:) ,dpsi_m(:,:)
 COMPLEX(DP), ALLOCATABLE :: ps(:,:)
 COMPLEX(DP), ALLOCATABLE :: dvpsi(:,:)
 COMPLEX(DP), ALLOCATABLE :: dv2_aux(:,:,:)
 REAL(DP), ALLOCATABLE, TARGET :: vxlocal(:)
 REAL(DP), POINTER :: vxlocals(:)
 ! exx potential from oep procedure
 INTEGER :: ibnd, jbnd, nbnd_eff
 REAL(DP) :: wg1, w0g, wgp, wwg, deltae, theta
 REAL(DP), EXTERNAL :: w0gauss, wgauss
 ! functions computing the delta and theta function

 CALL start_clock ('psi_2_bare')
 ALLOCATE ( ps(nbnd,nbnd) )
 ALLOCATE ( aux1( dffts%nnr) ,aux2( dffts%nnr), psi_r(dffts%nnr), phi_r(dffts%nnr) )
 ALLOCATE ( dpsi_p_r(dffts%nnr), dpsi_m_r(dffts%nnr) )
 ALLOCATE ( result1(dffts%nnr), result2(dffts%nnr) )
 ALLOCATE ( phi(npwx,nbnd), dpsi_p(npwx,nbnd), dpsi_m(npwx,nbnd) )
 ALLOCATE ( vc(dffts%nnr), rhoc(dffts%nnr) )
 ALLOCATE ( vc_p(dffts%nnr), vc_m(dffts%nnr) )
 ALLOCATE  ( rhoc_p(dffts%nnr), rhoc_m(dffts%nnr) )
 ALLOCATE ( fac(ngm) )
 ALLOCATE ( dpsi_1(npwx,nbnd,2) , dv2_aux(npwx,nbnd,2) )
 ALLOCATE ( dvpsi(npwx,nbnd) )
 ALLOCATE ( vxlocal(dfftp%nnr) )
 IF (doublegrid) THEN
    ALLOCATE (vxlocals ( dffts%nnr))
 ELSE
    vxlocals => vxlocal
 ENDIF
 !
 !
 !!!!------------------------------------------------------------------------!!!!
 !!                               compute term #1                              !! 
 !!   -sum_p Int dr' [psi_p^*(r') vc(r-r')psi_k(r') dpsi_plus/minus_p+q(r)]    !!
 !!!!------------------------------------------------------------------------!!!!
 !
 ! NsC >
 npw = ngk(ikk)
 npwq= ngk(ikq)
 !< 
 DO ibnd = 1, nbnd_occ(ikk)                                          !!! for each band in psi_a
    !
    psi_r(:) = (0.D0, 0.D0)  
!    psi_r(nls(igk(1:npw))) = evc(1:npw,ibnd)
    psi_r(nls(igk_k(1:npw,ikk))) = evc(1:npw,ibnd)
    CALL invfft( 'Wave', psi_r, dffts )                               !! wfc in real space
    !
!    IF (nksq.gt.1) rewind (unit = iunigk)
    !
    result1(:) = (0.D0, 0.D0)
    result2(:) = (0.D0, 0.D0)
    DO ip = 1, nksq                                                  !! sum over kpoint p
       !
!       IF (nksq.gt.1) THEN
!          READ (iunigk, err = 101, iostat = ios) npw, igk
!101       CALL errore ('rhs_dchi', 'reading igk', abs (ios) )
!       ENDIF
       !
       ipp = ikks(ip)                                                !! index of kpoint p
       ipq = ikqs(ip)                                                !! index of kpoint p+q
       ! NsC 
       npw = ngk (ipp)                                               !! # of pw at k point p
       npwq= ngk (ipq)                                               !! # of pw at k point p+q
       !
       IF (lsda) current_spin = isk (ipp)
!       IF ( .NOT. lgamma .AND. nksq .GT. 1) then
!          READ (iunigk, err = 201, iostat = ios) npwq, igkq
!201       CALL errore ('rhs_dchi', 'reading igkq', abs (ios) )
!       ENDIF
       !
       CALL g2_convolution(ngm, g, xk(:,ikk), xk(:,ipp), fac)        !!! calcola fpi e^2/|k-p+G|^2
       !
       IF ( nksq .GT. 1 ) THEN                                       !!! if # of kpoint > 1 
          !
          CALL davcio ( phi, lrwfc, iuwfc, ipp, - 1)                 !!! reads wfc at kpoint p
          CALL davcio ( dpsi_p, lrdwf, iudwf, ip, -1 )               !!! read dwfc_plus at kpoint p+q
          CALL davcio ( dpsi_m, lrdwf, iudwf, ip+nksq, -1 )          !!! read dwfc_minus at kpoint p+q 
          !
       ELSE                                                          !!! if # of kpoint = 1 wfc and dwfc stored in evc dpsi_plus and dpsi_minus
          !
          phi(:,:) = evc (:,:)
          dpsi_p (:,:) = dpsi_plus (:,:)
          dpsi_m (:,:) = dpsi_minus (:,:)
          !
       ENDIF
       !
       DO jbnd =1, nbnd_occ(ipp)                                     !!! summ over band
          !
          phi_r = (0.D0,0.D0) 
!          phi_r(nls(igk(1:npw))) = phi(1:npw,jbnd)
          phi_r(nls(igk_k(1:npw,ipp))) = phi(1:npw,jbnd)
          CALL invfft ( 'Wave', phi_r, dffts )                       !!! wfc at kpoint p in real space 
          !
          dpsi_p_r = (0.D0, 0.D0)
          dpsi_m_r = (0.D0, 0.D0)
!          dpsi_p_r(nls(igkq(1:npwq))) = dpsi_p(1:npwq,jbnd)               
          dpsi_p_r(nls(igk_k(1:npwq,ipq))) = dpsi_p(1:npwq,jbnd)               
!          dpsi_m_r(nls(igkq(1:npwq))) = dpsi_m(1:npwq,jbnd) 
          dpsi_m_r(nls(igk_k(1:npwq,ipq))) = dpsi_m(1:npwq,jbnd) 
          !
          CALL invfft ( 'Wave', dpsi_p_r, dffts )                    !!! dwfc_plus at kpoint p+q in real space 
          CALL invfft ( 'Wave', dpsi_m_r, dffts )                    !!! dwfc_minus at kpoint p+q in real space
          !
          rhoc(:) =(0.D0, 0.D0)
          rhoc(:) = ( CONJG(phi_r(:)) * psi_r(:) )/omega             !!! product wfc at p times wfc at k
          CALL fwfft('Smooth', rhoc, dffts)                          !!! goto reciprocal space
          !
          vc(:) = (0.D0, 0.D0)
          vc(nls(1:ngm)) = fac(1:ngm) * rhoc(nls(1:ngm))             !!! convolution \Int [phi(r') vc(r-r') psi(r')] dr'
          CALL invfft ('Smooth', vc, dffts)
          ! 
          ! Accumulates over band and kpoint
          !
          result1(1:dffts%nnr) = result1(1:dffts%nnr) + vc(1:dffts%nnr) * dpsi_p_r(1:dffts%nnr)
!          result1(1:dffts%nnr) = result1(1:dffts%nnr) + vc(1:dffts%nnr) * phi_r(1:dffts%nnr)
          result2(1:dffts%nnr) = result2(1:dffts%nnr) + vc(1:dffts%nnr) * dpsi_m_r(1:dffts%nnr)
          !
       ENDDO ! jband
       !
    ENDDO    ! ip
    !
    CALL fwfft( 'Wave', result1, dffts )
    CALL fwfft( 'Wave', result2, dffts )
!    WRITE(*,*) 'Vx', result1(nls(igk(1:3)))
!    dvpsi=(0.D0, 0.D0)
!    CALL vexx ( npwx, npw, nbnd_occ(ikk), evc, dvpsi )
!    WRITE(*,*) 'Vx routine', dvpsi(1:3,1)
!    WRITE(*,*) 'omega', omega
!    WRITE(*,*) 'rap',  result1(nls(igk(1:3)))/(dvpsi(1:3,1))
    ! NsC > Set back the # pw of k point k
    npw = ngk(ikk)
    npwq= ngk(ikq)
    ! <
!    dv2(1:npwq,ibnd,1) = dv2(1:npwq,ibnd,1) - result1(nls(igkq(1:npwq)))
    dv2(1:npwq,ibnd,1) = dv2(1:npwq,ibnd,1) - result1(nls(igk_k(1:npwq,ikq)))
!    dv2(1:npwq,ibnd,2) = dv2(1:npwq,ibnd,2) - result2(nls(igkq(1:npwq)))
    dv2(1:npwq,ibnd,2) = dv2(1:npwq,ibnd,2) - result2(nls(igk_k(1:npwq,ikq)))
 ENDDO !!! ibnd
 !
 !
 !!!!-------------------------------------------------------------!!!!
 !!                        compute term #2                          !!
 !!                [Vx_NL -vx]|dpsi_plus or minus>                  !!
 !!!!-------------------------------------------------------------!!!!
 !
 !
 ! compute Vx_Nl|dpsi> with dpsi^+/-; stored in dv2_aux
 ! CALL vexx (npwx, npw, nbnd_occ(ikk), evc, dvpsi )
 !
 dv2_aux(:,:,:) = (0.D0, 0.D0)
 !
 DO ibnd = 1, nbnd_occ(ikk)                                          !!! for each band in psi_k+q
    !
    ! NsC >
    npw = ngk(ikk)
    npwq= ngk(ikq)
    ! <
    dpsi_p_r(:) = (0.D0, 0.D0)  
    dpsi_m_r(:) = (0.D0, 0.D0)
!    dpsi_p_r(nls(igkq(1:npwq))) = dpsi_plus(1:npwq,ibnd)
    dpsi_p_r(nls(igk_k(1:npwq,ikq))) = dpsi_plus(1:npwq,ibnd)
!    dpsi_m_r(nls(igkq(1:npwq))) = dpsi_minus(1:npwq,ibnd)
    dpsi_m_r(nls(igk_k(1:npwq,ikq))) = dpsi_minus(1:npwq,ibnd)
    CALL invfft( 'Wave', dpsi_p_r, dffts )
    CALL invfft( 'Wave', dpsi_m_r, dffts )                            !! dwfc at k+q in real space
    !
!    IF (nksq.gt.1) rewind (unit = iunigk)
    !
    result1(:) = (0.D0, 0.D0)
    result2(:) = (0.D0, 0.D0)
    DO ip = 1, nksq                                                  !! sum over kpoint p
       !
!       IF (nksq.gt.1) THEN
!          READ (iunigk, err = 102, iostat = ios) npw, igk
!102       CALL errore ('rhs_dchi', 'reading igk', abs (ios) )
!       ENDIF
       !
       ipp = ikks(ip)                                                !! index of kpoint p
       ipq = ikqs(ip)                                                !! index ok kpoint p+q
       ! NsC >
       npw = ngk(ipp)                                                !! # of pw at kpoint p
       npwq= ngk(ipq)                                                !! # of pw at kpoint p+q
       ! <
       IF (lsda) current_spin = isk (ipp)
!       IF ( .NOT. lgamma .AND. nksq .GT. 1) then
!          READ (iunigk, err = 202, iostat = ios) npwq, igkq
!202       CALL errore ('rhs_dchi', 'reading igkq', abs (ios) )
!       ENDIF
       !
       CALL g2_convolution(ngm, g, xk(:,ikq), xk(:,ipp), fac)        !!! calcola fpi e^2/|k-p+G|^2
       !
       IF ( nksq .GT. 1 ) THEN                                       !!! if # of kpoint > 1 
          !
          CALL davcio ( phi, lrwfc, iuwfc, ipp, - 1)                 !!! reads wfc at kpoint p
!          CALL davcio ( dpsi_p, lrdwf, iudwf, ip, -1 )               !!! read dwfc_plus at kpoint p+q
!          CALL davcio ( dpsi_m, lrdwf, iudwf, ip+nksq, -1 )          !!! read dwfc_minus at kpoint p+q 
          !
       ELSE                                                          !!! if # of kpoint = 1 wfc and dwfc stored in evc dpsi_plus and dpsi_minus
          !
          phi(:,:) = evc (:,:)
!          dpsi_p (:,:) = dpsi_plus (:,:)
!          dpsi_m (:,:) = dpsi_minus (:,:)
          !
       ENDIF
       !
       DO jbnd =1, nbnd_occ(ipp)                                     !!! summ over band
          !
          phi_r = (0.D0,0.D0) 
!          phi_r(nls(igk(1:npw))) = phi(1:npw,jbnd)
          phi_r(nls(igk_k(1:npw,ipp))) = phi(1:npw,jbnd)
          CALL invfft ( 'Wave', phi_r, dffts )                       !!! wfc at kpoint p in real space 
          !
!          dpsi_p_r = (0.D0, 0.D0)
!          dpsi_m_r = (0.D0, 0.D0)
!          dpsi_p_r(nls(igkq(1:npwq))) = dpsi_p(1:npwq,jbnd)               
!          dpsi_m_r(nls(igkq(1:npwq))) = dpsi_m(1:npwq,jbnd) 
          !
!          CALL invfft ( 'Wave', dpsi_p_r, dffts )                    !!! dwfc_plus at kpoint p+q in real space 
!          CALL invfft ( 'Wave', dpsi_m_r, dffts )                    !!! dwfc_minus at kpoint p+q in real space
          !
          rhoc_p(:) =(0.D0, 0.D0)
          rhoc_m(:) =(0.D0, 0.D0)
          rhoc_p(:) = ( CONJG(phi_r(:)) * dpsi_p_r(:) )/omega        !!! product dwfc at p+q times wfc at k+q
          rhoc_m(:) = ( CONJG(phi_r(:)) * dpsi_m_r(:) )/omega        !!! product dwfc at p+q times wfc at k+q
          CALL fwfft('Smooth', rhoc_p, dffts )                       !!! goto reciprocal space
          CALL fwfft('Smooth', rhoc_m, dffts )
          !
          vc_p(:) = (0.D0, 0.D0)
          vc_m(:) = (0.D0, 0.D0)
          vc_p(nls(1:ngm)) = fac(1:ngm) * rhoc_p(nls(1:ngm))             !!! convolution \Int [dpsi_p(r') vc(r-r') psi(r')] dr'
          vc_m(nls(1:ngm)) = fac(1:ngm) * rhoc_m(nls(1:ngm))             !!! convolution \Int [dpsi_m(r') vc(r-r') psi(r')] dr'
          CALL invfft ('Smooth', vc_p, dffts )
          CALL invfft ('Smooth', vc_m, dffts )
          ! 
          ! Accumulates over band and kpoint
          !
          result1(1:dffts%nnr) = result1(1:dffts%nnr) + vc_p(1:dffts%nnr) * phi_r(1:dffts%nnr)
!          result1(1:dffts%nnr) = result1(1:dffts%nnr) + vc(1:dffts%nnr) * phi_r(1:dffts%nnr)
          result2(1:dffts%nnr) = result2(1:dffts%nnr) + vc_m(1:dffts%nnr) * phi_r(1:dffts%nnr)
          !
       ENDDO ! jband
       !
    ENDDO    ! ip
    !
    CALL fwfft( 'Wave', result1, dffts )
    CALL fwfft( 'Wave', result2, dffts )
!    WRITE(*,*) 'Vx', result1(nls(igk(1:3)))
!    dvpsi=(0.D0, 0.D0)
!    CALL vexx ( npwx, npw, nbnd_occ(ikk), evc, dvpsi )
!    WRITE(*,*) 'Vx routine', dvpsi(1:3,1)
!    WRITE(*,*) 'omega', omega
!    WRITE(*,*) 'rap',  result1(nls(igk(1:3)))/(dvpsi(1:3,1))
    ! NsC > Set back the correct # of pw at kpoint point k
    npw = ngk(ikk)
    npwq= ngk(ikq)
!    dv2_aux(1:npwq,ibnd,1) = dv2_aux(1:npwq,ibnd,1) - result1(nls(igkq(1:npwq)))
    dv2_aux(1:npwq,ibnd,1) = dv2_aux(1:npwq,ibnd,1) - result1(nls(igk_k(1:npwq,ikq)))
!    dv2_aux(1:npwq,ibnd,2) = dv2_aux(1:npwq,ibnd,2) - result2(nls(igkq(1:npwq)))
    dv2_aux(1:npwq,ibnd,2) = dv2_aux(1:npwq,ibnd,2) - result2(nls(igk_k(1:npwq,ikq)))
 ENDDO !!! ibnd
 !
 ! compute vx|dpsi> with dpsi^+/-
 vxlocal(:) = 0.D0 
 vxlocal (:) = exxvx (:)
 !
 ! NsC > Set back the correct # of pw for point k and k+q
 npw = ngk(ikk)
 npwq= ngk(ikq)
 ! <
 IF (doublegrid) CALL interpolate(vxlocal(1),vxlocals(1),-1)
 DO ibnd = 1, nbnd_occ(ikk)
    aux1(:) = (0.D0, 0.D0)
    aux2(:) = (0.D0, 0.D0)
    DO ig = 1, npwq
!       aux1(nls(igkq(ig))) = dpsi_plus(ig,ibnd)
       aux1(nls(igk_k(ig,ikq))) = dpsi_plus(ig,ibnd)
!       aux2(nls(igkq(ig))) = dpsi_minus(ig,ibnd)
       aux2(nls(igk_k(ig,ikq))) = dpsi_minus(ig,ibnd)
    ENDDO
    !
    CALL invfft ('Wave' , aux1, dffts)
    CALL invfft ('Wave' , aux2, dffts)
    !
    DO ir =1, dffts%nnr                          !!! CALCOLA vxlocal(r)*dpsi_plus(r) and minus
        aux1(ir) = aux1(ir)*vxlocals(ir) 
        aux2(ir) = aux2(ir)*vxlocals(ir)
    ENDDO
    CALL fwfft('Wave', aux1, dffts)
    CALL fwfft('Wave', aux2, dffts)
    DO ig = 1, npwq                             !!! CALCOLA dvpsi_NL - dvdps_L (il primo e' in dvpsi il secondo in aux1)
!        dv2_aux(ig, ibnd,1) = dv2_aux(ig,ibnd,1) - aux1(nls(igkq(ig)))
        dv2_aux(ig, ibnd,1) = dv2_aux(ig,ibnd,1) - aux1(nls(igk_k(ig,ikq)))
!        dv2_aux(ig, ibnd,2) = dv2_aux(ig,ibnd,2) - aux2(nls(igkq(ig)))
        dv2_aux(ig, ibnd,2) = dv2_aux(ig,ibnd,2) - aux2(nls(igk_k(ig,ikq)))
    ENDDO
 ENDDO
 !
 ! add the result to dv2
 !
 dv2(:,:,:) = dv2(:,:,:) + dv2_aux(:,:,:)
 !
 !RETURN
 !
 !!!!------------------------------------------------------------!!!!
 !!                         compute term #3                        !! 
 !!            -sum_b dpsi^+/-_b <psi_b|[Vx-vx]|psi_a>             !!
 !!!!------------------------------------------------------------!!!!
 !
 !
 ! conpute [Vx-vx]|psi>
 !
 dvpsi(:,:) = (0.D0, 0.D0)
 !
 CALL vexx( npwx, npw, nbnd_occ(ikk), evc, dvpsi )
 !
 ! NsC > Set back the correct # of pw for point k and k+q
 npw = ngk(ikk)
 npwq= ngk(ikq)
 ! <
 DO ibnd = 1, nbnd_occ(ikk)
    aux1(:) = (0.D0, 0.D0)
    DO ig = 1, npw
!       aux1(nls(igk(ig))) = evc(ig,ibnd)
       aux1(nls(igk_k(ig,ikk))) = evc(ig,ibnd)
    ENDDO
    CALL invfft ('Wave' , aux1, dffts)
    DO ir =1, dffts%nnr                   !!!!!! CALCOLA vxlocal(r)*psi(r)
       aux1(ir) = aux1(ir)*vxlocals(ir)   
    ENDDO
    CALL fwfft('Wave', aux1, dffts)
    DO ig = 1, npw                        !!! CALCOLA dvpsi_NL - dvdps_L (il primo e' in dvpsi il secondo in aux1)
!       dvpsi(ig, ibnd) = dvpsi(ig,ibnd) - aux1(nls(igk(ig)))
       dvpsi(ig, ibnd) = dvpsi(ig,ibnd) - aux1(nls(igk_k(ig,ikk)))
    ENDDO
 ENDDO
 ! 
 !
 !  insulators
 !
 !
 ps = (0.d0, 0.d0)
 IF (noncolin) THEN
    CALL zgemm( 'C', 'N',nbnd_occ(ikq), nbnd_occ(ikk), npwx*npol, &
           (1.d0,0.d0), evc, npwx*npol, dvpsi, npwx*npol, &
           (0.d0,0.d0), ps, nbnd )
 ELSE
 !      CALL zgemm( 'C', 'N', nbnd_occ(ikq), nbnd_occ (ikk), npwq, &
 !             (1.d0,0.d0), evc, npwx, dvpsi, npwx, &
 !             (0.d0,0.d0), ps, nbnd )
       CALL zgemm( 'C', 'N', nbnd_occ(ikk), nbnd_occ (ikk), npw, &
              (1.d0,0.d0), evc, npwx, dvpsi, npwx, &
              (0.d0,0.d0), ps, nbnd )
 END IF
 nbnd_eff=nbnd_occ(ikk)
 !
#if defined __MPI
    call mp_sum(ps(:,1:nbnd_eff),intra_pool_comm)
#endif
 !
 ! dpsi_1 is used as work space to store S|dpsi>

 !IF (okvan) CALL calbec ( npwq, vkb, evq, becp, nbnd_eff)
 ! notsure right or wrong
 !
 dpsi_1(:,:,:) = (0.D0, 0.D0)
 CALL s_psi (npwx, npwq, nbnd_eff, dpsi_plus, dpsi_1(1,1,1) )
 CALL s_psi (npwx, npwq, nbnd_eff, dpsi_minus, dpsi_1(1,1,2) )
 !
 ! |dvdpsi> =  (|dvdpsi> - S|dpsi^+/-><evc|dvpsi>)
 !
 !
 !  Insulators: note that nbnd_occ(ikk)=nbnd_occ(ikq) in an insulator
 !
 IF (noncolin) THEN
    CALL zgemm( 'N', 'N', npwx*npol, nbnd_occ(ikk), nbnd_occ(ikk), &
              (-1.d0,0.d0),dpsi_1(1,1,1),npwx*npol,ps,nbnd,(1.0d0,0.d0), &
              dv2(1,1,1), npwx*npol )
    CALL zgemm( 'N', 'N', npwx*npol, nbnd_occ(ikk), nbnd_occ(ikk), &
              (-1.d0,0.d0),dpsi_1(1,1,2),npwx*npol,ps,nbnd,(1.0d0,0.d0), &
              dv2(1,1,2), npwx*npol )
 ELSE
    !CALL zgemm( 'N', 'N', npwq, nbnd_occ(ikk), nbnd_occ(ikk), &
    CALL zgemm( 'N', 'N', npwq, nbnd_occ(ikk), nbnd_occ(ikk), &
           (-1.d0,0.d0), dpsi_1(1,1,1), npwx, ps, nbnd, (1.0d0,0.d0), &
            dv2(1,1,1), npwx )
    CALL zgemm( 'N', 'N', npwq, nbnd_occ(ikk), nbnd_occ(ikk), &
           (-1.d0,0.d0), dpsi_1(1,1,2), npwx, ps, nbnd, (1.0d0,0.d0), &
            dv2(1,1,2), npwx )
 END IF
 !
 !RETURN
 !
 !!!!-------------------------------------------------------------!!!!
 !!                         compute term #4                         !!
 !!                            DV *dpsi_nl                          !!
 !!!!-------------------------------------------------------------!!!!
 !
 ! compute DV|dpsi_nl>
 !
 ! NsC > Set back the correct # of pw for point k and k+q
 npw = ngk(ikk)
 npwq= ngk(ikq)
 ! <
 do ibnd = 1, nbnd_occ (ikk)
    aux1(:) = (0.d0, 0.d0)
    do ig = 1, npw
!       aux1 (nls(igk(ig))) = dpsi_nl(ig,ibnd)
       aux1 (nls(igk_k(ig,ikk))) = dpsi_nl(ig,ibnd)
    enddo
    CALL invfft ('Wave', aux1, dffts)
    do ir = 1, dffts%nnr
         aux1(ir) = aux1(ir) * dv(ir,current_spin,1)
    enddo
    !
    CALL fwfft ('Wave', aux1, dffts)
    do ig = 1, npwq
!        dv2(ig,ibnd,1) = dv2(ig,ibnd,1) + aux1(nls(igkq(ig)))
        dv2(ig,ibnd,1) = dv2(ig,ibnd,1) + aux1(nls(igk_k(ig,ikq)))
!        dv2(ig,ibnd,2) = dv2(ig,ibnd,2) + aux1(nls(igkq(ig)))
        dv2(ig,ibnd,2) = dv2(ig,ibnd,2) + aux1(nls(igk_k(ig,ikq)))
    enddo
 enddo
 !
 !RETURN
 !
 !
 !!!!-------------------------------------------------------------!!!!
 !!                       compute term #5                           !!
 !!           -sum_b dpsi_nl_b <psi_b| DV |psi_a>                   !!
 !!!!-------------------------------------------------------------!!!!
 !
 !
 ! conpute DV|psi>
 !
 ! NsC > Set back the correct # of pw for point k and k+q
 npw = ngk(ikk)
 npwq= ngk(ikq)
 ! <
 dvpsi(:,:) = (0.D0, 0.D0)
 do ibnd = 1, nbnd_occ (ikk)
    aux1(:) = (0.d0, 0.d0)
    do ig = 1, npw
!       aux1 (nls(igk(ig))) = evc(ig,ibnd)
       aux1 (nls(igk_k(ig,ikk))) = evc(ig,ibnd)
    enddo
    CALL invfft ('Wave', aux1, dffts)
    do ir = 1, dffts%nnr
         aux1(ir) = aux1(ir) * dv(ir,current_spin,1)
    enddo
    !
    CALL fwfft ('Wave', aux1, dffts)
    do ig = 1, npwq
!        dvpsi(ig,ibnd) =  aux1(nls(igkq(ig)))
        dvpsi(ig,ibnd) =  aux1(nls(igk_k(ig,ikq)))
    enddo
 enddo
 ! 
 !
 !  insulators
 !
 !  in ps it's stored the product <psi_b|DV|psi_a>
 !
 ps = (0.d0, 0.d0) 
 IF (noncolin) THEN
    CALL zgemm( 'C', 'N',nbnd_occ(ikq), nbnd_occ(ikk), npwx*npol, &
           (1.d0,0.d0), evq, npwx*npol, dvpsi, npwx*npol, &
           (0.d0,0.d0), ps, nbnd )
 ELSE
 !      CALL zgemm( 'C', 'N', nbnd_occ(ikq), nbnd_occ (ikk), npwq, &
 !             (1.d0,0.d0), evc, npwx, dvpsi, npwx, &
 !             (0.d0,0.d0), ps, nbnd )
       CALL zgemm( 'C', 'N', nbnd_occ(ikq), nbnd_occ (ikk), npwq, &
              (1.d0,0.d0), evq, npwx, dvpsi, npwx, &
              (0.d0,0.d0), ps, nbnd )
 END IF
 nbnd_eff=nbnd_occ(ikk)
 !
#if defined __MPI
    call mp_sum(ps(:,1:nbnd_eff),intra_pool_comm)
#endif
 !
 ! dpsi_1 is used as work space to store S|dpsi>
 ! For ultrasoft pseudo S is needed
 !
 dpsi_1(:,:,:) =(0.D0, 0.D0)
 
 !IF (okvan) CALL calbec ( npwq, vkb, evq, becp, nbnd_eff)
 ! notsure right or wrong
 !
 CALL s_psi (npwx, npwq, nbnd_eff, dpsiq_nl, dpsi_1(1,1,1) )
 dpsi_1(:,:,2) = dpsi_1(:,:,1)
 !
 ! |dvdpsi> =  (|dvdpsi> - S|dpsi_nl><evc|dvpsi>)
 !
 !
 !  Insulators: note that nbnd_occ(ikk)=nbnd_occ(ikq) in an insulator
 !  compute dv2 - sum_b [|dpsi_nl_b><psi_b|DV\psi_a>]
 !
 IF (noncolin) THEN
    CALL zgemm( 'N', 'N', npwx*npol, nbnd_occ(ikk), nbnd_occ(ikk), &
              (-1.d0,0.d0),dpsi_1(1,1,1),npwx*npol,ps,nbnd,(1.0d0,0.d0), &
              dv2(1,1,1), npwx*npol )
    CALL zgemm( 'N', 'N', npwx*npol, nbnd_occ(ikk), nbnd_occ(ikk), &
              (-1.d0,0.d0),dpsi_1(1,1,2),npwx*npol,ps,nbnd,(1.0d0,0.d0), &
              dv2(1,1,2), npwx*npol )
 ELSE
    !CALL zgemm( 'N', 'N', npwq, nbnd_occ(ikk), nbnd_occ(ikk), &
    CALL zgemm( 'N', 'N', npwq, nbnd_occ(ikk), nbnd_occ(ikq), &
           (-1.d0,0.d0), dpsi_1(1,1,1), npwx, ps, nbnd, (1.0d0,0.d0), &
            dv2(1,1,1), npwx )
    CALL zgemm( 'N', 'N', npwq, nbnd_occ(ikk), nbnd_occ(ikq), &
           (-1.d0,0.d0), dpsi_1(1,1,2), npwx, ps, nbnd, (1.0d0,0.d0), &
            dv2(1,1,2), npwx )
 END IF
 !
 !
 !RETURN
 !WRITE(*,*) 'prova'


 DEALLOCATE ( ps )
 DEALLOCATE ( phi, psi_r, phi_r, dpsi_m, dpsi_p, dpsi_m_r, dpsi_p_r, vc, rhoc, fac )
 DEALLOCATE ( rhoc_p, rhoc_m, vc_p, vc_m )
 DEALLOCATE ( result1, result2 )
 DEALLOCATE ( dpsi_1, dv2_aux, dvpsi )
 DEALLOCATE ( vxlocal )
 IF (doublegrid)  DEALLOCATE (vxlocals)
 DEALLOCATE ( aux1, aux2 )
 CALL stop_clock ('psi_2_bare')
 !
 RETURN
 END SUBROUTINE rhs_dchi_rpax



!-----------------------------------------------------------------------
SUBROUTINE rhs2_dchi_rpax (ikk, ikq, evc, evq, dpsi_plus, dpsi_minus, dv2)
!------------------------------------------------------------------------
  !
  ! This routine to calculate rhs of linear eqs of |psi_2>.
  ! |dvdpsi_v> = |dvdpsi_v> - S|dpsi_v'><psi_v'|(DV)*|psi_v>  (1)
  ! This routine will do: 
  !        1) ps = <psi_v'|dvpsi_v>
  !        2) calc. S|dpsi_v'>
  !        3) apply (1) to find |dvdpsi>
  ! In general, this routine works like routine orthogonalize ()
  !
  ! NB: INPUT is dvpsi and dpsi ; 
  !   : dpsi_1 is used as work_space of S|dpsi> calculation
  !   : OUTPUT is dvdpsi
  !
  USE kinds,                 ONLY : DP
  USE lsda_mod,              ONLY : lsda, isk, current_spin, nspin
  USE klist,                 ONLY : xk, ngk, igk_k
  USE cell_base,             ONLY : omega
  USE noncollin_module,      ONLY : noncolin, npol
  USE wvfct,                 ONLY : npwx, nbnd
  USE gvect,                 ONLY : g, ngm
  USE gvecs,                 ONLY : nls
  USE qpoint,                ONLY : nksq, ikks, ikqs
  USE control_lr,            ONLY : nbnd_occ
  USE mp_global,             ONLY : intra_pool_comm
  USE mp,                    ONLY : mp_sum
  USE units_ph,              ONLY : iudwf, iuwfc, lrdwf, lrwfc
  USE fft_base,              ONLY : dffts, dfftp
  USE fft_interfaces,        ONLY : fwfft, invfft
  USE control_acfdt,         ONLY : exxvx
  USE exx,                   ONLY : vexx, g2_convolution

!
 IMPLICIT NONE
 !INTEGER, INTENT(IN) :: ikk, ikq   ! the index of the k and k+q points
 !COMPLEX(DP), INTENT(IN) :: evc(npwx*npol,nbnd)
 !COMPLEX(DP), INTENT(IN) :: dvpsi(npwx*npol,nbnd)
 !COMPLEX(DP), INTENT(INOUT) :: dvdpsi(npwx*npol,nbnd)
 !COMPLEX(DP), INTENT(IN) :: dpsi  (npwx*npol,nbnd)
! COMPLEX(DP), ALLOCATABLE :: dpsi_1(:,:,:) ! work space allocated by
                                                    ! the calling routine

 integer, parameter :: npe = 1            
 INTEGER, INTENT(IN) :: ikk ,ikq
 INTEGER :: npw, npwq
 COMPLEX(DP), INTENT(IN) :: evq(npwx,nbnd), evc(npwx,nbnd)
 COMPLEX(DP), INTENT(IN) :: dpsi_plus(npwx,nbnd), dpsi_minus(npwx,nbnd)
! COMPLEX(DP), INTENT(IN) ::dpsi_nl(npwx,nbnd), dpsiq_nl(npwx,nbnd)
 COMPLEX(DP), INTENT(INOUT) :: dv2(npwx,nbnd,2)
 ! inout: rhs of linear system for dpsi2
! COMPLEX(DP), INTENT(IN) :: dv(dffts%nnr , nspin , npe)
! ! input: dynamical potential

 integer ::  ig,         & ! counter on G vectors
             ir,         & ! counter on mesh points
             is,         & ! counter on spin polarizations
             ip,         & ! counter kpoint
             ios,        &
             ipp,        & ! index of kpoint p
             ipq           ! index of kpoint p+q

! COMPLEX(DP), ALLOCATABLE :: aux1(:), aux2(:)
 COMPLEX(DP), ALLOCATABLE :: psi_r(:), phi_r(:), dpsi_p_r(:), dpsi_m_r(:)
 COMPLEX(DP), ALLOCATABLE :: result1(:), result2(:), vc_p(:), vc_m(:), rhoc_p(:), rhoc_m(:)
 REAL(DP) , ALLOCATABLE :: fac(:)
 COMPLEX(DP), ALLOCATABLE :: phi(:,:) , dpsi_p(:,:) ,dpsi_m(:,:)
! COMPLEX(DP), ALLOCATABLE :: dvpsi(:,:)
! COMPLEX(DP), ALLOCATABLE :: ps(:,:)


 INTEGER :: ibnd, jbnd, nbnd_eff
 REAL(DP) :: wg1, w0g, wgp, wwg, deltae, theta
 REAL(DP), EXTERNAL :: w0gauss, wgauss
 ! functions computing the delta and theta function

 CALL start_clock ('psi_2_bare')

 ALLOCATE ( psi_r(dffts%nnr), phi_r(dffts%nnr) )
 ALLOCATE ( dpsi_p_r(dffts%nnr), dpsi_m_r(dffts%nnr) )
 ALLOCATE ( result1(dffts%nnr), result2(dffts%nnr) )
 ALLOCATE ( phi(npwx,nbnd), dpsi_p(npwx,nbnd), dpsi_m(npwx,nbnd) )
 ALLOCATE ( vc_p(dffts%nnr), vc_m(dffts%nnr), rhoc_p(dffts%nnr), rhoc_m(dffts%nnr) )
 ALLOCATE ( fac(ngm) )

 !
 !!!!------------------------------------------------------------------------!!!!
 !!                               compute term #1                              !! 
 !!   -sum_p Int dr' [dpsi_plus/minus_p+q^*(r') vc(r-r')psi_k+q(r') psi_p(r)]  !!
 !!!!------------------------------------------------------------------------!!!!
 !
 ! NsC > Set back the correct # of pw for point k and k+q
 npw = ngk(ikk)
 npwq= ngk(ikq)
 ! <
 DO ibnd = 1, nbnd_occ(ikk)                                          !!! for each band in psi_k+q
    !
    psi_r(:) = (0.D0, 0.D0)  
!    psi_r(nls(igkq(1:npwq))) = evq(1:npwq,ibnd)
    psi_r(nls(igk_k(1:npwq,ikq))) = evq(1:npwq,ibnd)
    CALL invfft( 'Wave', psi_r, dffts)                               !! wfc at k+q in real space
    !
!    IF (nksq.gt.1) rewind (unit = iunigk)
    !
    result1(:) = (0.D0, 0.D0)
    result2(:) = (0.D0, 0.D0)
    DO ip = 1, nksq                                                  !! sum over kpoint p
       !
!       IF (nksq.gt.1) THEN
!          READ (iunigk, err = 100, iostat = ios) npw, igk
!100       CALL errore ('rhs_dchi', 'reading igk', abs (ios) )
!       ENDIF
       !
       ipp = ikks(ip)                                                !! index of kpoint p
       ipq = ikqs(ip)                                                !! index ok kpoint p+q
       ! Nsc >
       npw = ngk(ipp)                                                !! # of pw at kpoint p
       npwq= ngk(ipp)                                                !! # of pw at kpoint p+q
       ! <
       IF (lsda) current_spin = isk (ipp)
!       IF ( .NOT. lgamma .AND. nksq .GT. 1) then
!          READ (iunigk, err = 200, iostat = ios) npwq, igkq
!200       CALL errore ('rhs_dchi', 'reading igkq', abs (ios) )
!       ENDIF
       !
       CALL g2_convolution(ngm, g, xk(:,ikq), xk(:,ipq), fac)        !!! calcola fpi e^2/|k-p+G|^2
       !
       IF ( nksq .GT. 1 ) THEN                                       !!! if # of kpoint > 1 
          !
          CALL davcio ( phi, lrwfc, iuwfc, ipp, - 1)                 !!! reads wfc at kpoint p
          CALL davcio ( dpsi_p, lrdwf, iudwf, ip, -1 )               !!! read dwfc_plus at kpoint p+q
          CALL davcio ( dpsi_m, lrdwf, iudwf, ip+nksq, -1 )          !!! read dwfc_minus at kpoint p+q 
          !
       ELSE                                                          !!! if # of kpoint = 1 wfc and dwfc stored in evc dpsi_plus and dpsi_minus
          !
          phi(:,:) = evc (:,:)
          dpsi_p (:,:) = dpsi_plus (:,:)
          dpsi_m (:,:) = dpsi_minus (:,:)
          !
       ENDIF
       !
       DO jbnd =1, nbnd_occ(ipp)                                     !!! summ over band
          !
          phi_r = (0.D0,0.D0) 
!          phi_r(nls(igk(1:npw))) = phi(1:npw,jbnd)
          phi_r(nls(igk_k(1:npw,ipp))) = phi(1:npw,jbnd)
          CALL invfft ( 'Wave', phi_r, dffts )                       !!! wfc at kpoint p in real space 
          !
          dpsi_p_r = (0.D0, 0.D0)
          dpsi_m_r = (0.D0, 0.D0)
!          dpsi_p_r(nls(igkq(1:npwq))) = dpsi_p(1:npwq,jbnd)               
          dpsi_p_r(nls(igk_k(1:npwq,ipq))) = dpsi_p(1:npwq,jbnd)               
!          dpsi_m_r(nls(igkq(1:npwq))) = dpsi_m(1:npwq,jbnd) 
          dpsi_m_r(nls(igk_k(1:npwq,ipq))) = dpsi_m(1:npwq,jbnd) 
          !
          CALL invfft ( 'Wave', dpsi_p_r, dffts )                    !!! dwfc_plus at kpoint p+q in real space 
          CALL invfft ( 'Wave', dpsi_m_r, dffts )                    !!! dwfc_minus at kpoint p+q in real space
          !
          rhoc_p(:) =(0.D0, 0.D0)
          rhoc_m(:) =(0.D0, 0.D0)
          rhoc_p(:) = ( CONJG(dpsi_p_r(:)) * psi_r(:) )/omega        !!! product dwfc at p+q times wfc at k+q
          rhoc_m(:) = ( CONJG(dpsi_m_r(:)) * psi_r(:) )/omega        !!! product dwfc at p+q times wfc at k+q
          CALL fwfft('Smooth', rhoc_p, dffts)                        !!! goto reciprocal space
          CALL fwfft('Smooth', rhoc_m, dffts)
          !
          vc_p(:) = (0.D0, 0.D0)
          vc_m(:) = (0.D0, 0.D0)
          vc_p(nls(1:ngm)) = fac(1:ngm) * rhoc_p(nls(1:ngm))             !!! convolution \Int [dpsi_p(r') vc(r-r') psi(r')] dr'
          vc_m(nls(1:ngm)) = fac(1:ngm) * rhoc_m(nls(1:ngm))             !!! convolution \Int [dpsi_m(r') vc(r-r') psi(r')] dr'
          CALL invfft ('Smooth', vc_p, dffts)
          CALL invfft ('Smooth', vc_m, dffts)
          ! 
          ! Accumulates over band and kpoint
          !
          result1(1:dffts%nnr) = result1(1:dffts%nnr) + vc_p(1:dffts%nnr) * phi_r(1:dffts%nnr)
!          result1(1:dffts%nnr) = result1(1:dffts%nnr) + vc(1:dffts%nnr) * phi_r(1:dffts%nnr)
          result2(1:dffts%nnr) = result2(1:dffts%nnr) + vc_m(1:dffts%nnr) * phi_r(1:dffts%nnr)
          !
       ENDDO ! jband
       !
    ENDDO    ! ip
    !
    CALL fwfft( 'Wave', result1, dffts )
    CALL fwfft( 'Wave', result2, dffts )
!    WRITE(*,*) 'Vx', result1(nls(igk(1:3)))
!    dvpsi=(0.D0, 0.D0)
!    CALL vexx ( npwx, npw, nbnd_occ(ikk), evc, dvpsi )
!    WRITE(*,*) 'Vx routine', dvpsi(1:3,1)
!    WRITE(*,*) 'omega', omega
!    WRITE(*,*) 'rap',  result1(nls(igk(1:3)))/(dvpsi(1:3,1))
    ! NsC > Set back the correct number of pw at k point k 
    npw = ngk (ikk)
    npwq= ngk (ikq) 
!    dv2(1:npw,ibnd,1) = dv2(1:npw,ibnd,1) - result1(nls(igk(1:npw)))
    dv2(1:npw,ibnd,1) = dv2(1:npw,ibnd,1) - result1(nls(igk_k(1:npw,ikk)))
!    dv2(1:npw,ibnd,2) = dv2(1:npw,ibnd,2) - result2(nls(igk(1:npw)))
    dv2(1:npw,ibnd,2) = dv2(1:npw,ibnd,2) - result2(nls(igk_k(1:npw,ikk)))
 ENDDO !!! ibnd


 DEALLOCATE ( psi_r, phi_r )
 DEALLOCATE ( dpsi_p_r, dpsi_m_r )
 DEALLOCATE ( result1, result2 )
 DEALLOCATE ( phi, dpsi_p, dpsi_m )
 DEALLOCATE ( vc_p, vc_m, rhoc_p, rhoc_m )
 DEALLOCATE ( fac )

 RETURN

END SUBROUTINE rhs2_dchi_rpax
