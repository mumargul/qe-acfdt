
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!------------------------------------------------------------------
SUBROUTINE vc12_dvg( ndim, m, xq, dvg)
 !
 ! This routine computes results of applying Vc to a potential
 ! associated with vector q. Coulomb cutoff technique can be used
 !
 USE kinds,         ONLY : dp
 USE io_global,     ONLY : stdout
 USE constants,     ONLY : fpi, e2
 USE cell_base,     ONLY : tpiba2, tpiba
 USE gvect,         ONLY : ngm, g, nl, gstart, nlm
 USE control_acfdt, ONLY : rpax
 USE exx,           ONLY : g2_convolution
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
 INTEGER, INTENT(IN) :: ndim, m
 ! input: dimension of the arrays for potential and charge density
 ! input: number of pot. 
 REAL(DP), INTENT(IN) :: xq(3)
 ! input: coordinates of q-vector
 COMPLEX(DP), INTENT(INOUT) :: dvg(ndim,m)
 ! inout: potentials in G-space (will be changed on exit)
 !
 ! local variables
 !
 REAL(DP) :: qg, qg2, fac1, fac(ndim)
 INTEGER :: i, ig
 REAL(DP) :: xkq(3), xk(3)
 !
 ! various checks
 !
 IF ( ndim .GT. ngm ) CALL errore('vc12_dvg', 'ndim too large', 1)
 IF ( ndim .LE. 0   ) CALL errore('vc12_dvg', 'ndim negative!', 1)
 !
 IF (rpax) THEN 
    !
    ! If RPAx the coulomb kernel has been inizialized in callexx_rpax
    ! and here we use a consistent definition. 
    !
    DO i = 1, m
      xk(:) = 0.D0
      xkq(:) = xk(:)
      CALL g2_convolution(ndim, g, xk, xkq, fac)
      DO ig =1, ndim
         dvg(ig,i) = dvg(ig,i) * cmplx(sqrt(fac(ig)), 0.d0) 
      ENDDO
   ENDDO
   !
   RETURN
   !
 ENDIF
 !
 DO i = 1, m
    !
    DO ig = 1, ndim
       qg2 = (g(1,ig)+xq(1))**2 + (g(2,ig)+xq(2))**2 + (g(3,ig)+xq(3))**2
       IF (qg2 > 1.d-10) THEN 
          fac1 = (e2*fpi)/(qg2*tpiba2)
          dvg(ig,i) = dvg(ig,i) * cmplx(sqrt(fac1), 0.d0)
       ENDIF
    ENDDO
    !
 ENDDO
 !
 RETURN
 !
END SUBROUTINE vc12_dvg
