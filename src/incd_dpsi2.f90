!
! Copyright (C) 2001-2008 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
subroutine incd_dpsi2 (summ, weight, ik, dpsi_plus, dpsi_minus)
  !-----------------------------------------------------------------------
  !
  !     This routine computes the third part of fomular:
  !     \sum_{v,v'} {<\dpsi_v+|\dpsi_v'-><\psi*_v'(r)}|dv|\psi_v(r)>}
  !     INPUT: dpsi - The perturbed wfc
  !     OUTPUT: summ - the final calculation 
  !
  USE kinds,                  ONLY : DP
  USE cell_base,              ONLY : omega
  USE fft_base,               ONLY : dffts
  USE fft_interfaces,         ONLY : invfft
  USE gvecs,                  ONLY : nls
  USE wvfct,                  ONLY : npwx, nbnd
  USE wavefunctions_module,   ONLY : evc
  USE qpoint,                 ONLY : ikks, ikqs
  USE control_lr,             ONLY : nbnd_occ
  USE mp_global,              ONLY : intra_pool_comm
  USE mp,                     ONLY : mp_sum
  USE klist,                  ONLY : ngk, igk_k
  !
  implicit none
  ! I/O variables
  COMPLEX(DP), INTENT (INOUT) :: summ (dffts%nnr) 
  !
  real(DP), INTENT (IN) :: weight
  ! input: the weight of the k point  
  INTEGER, INTENT(IN) :: ik
  ! input: the k point
  complex(DP), INTENT (IN) :: dpsi_plus (npwx,nbnd)
  complex(DP), INTENT (IN) :: dpsi_minus(npwx,nbnd)
  ! input: the perturbed wfc for the given k point for +/- iu
  !
  ! Here the local variable
  !
  real(DP) :: wgt
  ! the effective weight of the k point
  !
  ! the wavefunctions in real space
  complex(DP), allocatable  :: psir (:,:)
  complex(DP), allocatable  :: ps (:,:) 
  !
  complex(DP) :: ps_1
  !
  integer :: ibnd, jbnd, ikk, ir, ig, nbnd_eff, ikq
  ! NsC >
  integer :: npw, npwq
  ! counters

  call start_clock ('incd_dpsi2')
  allocate (psir (  dffts%nnr, nbnd))
  allocate (ps   (  nbnd , nbnd))
  ikk = ikks(ik)
  ikq = ikqs(ik)
  ! NsC >
  npw = ngk(ikk)
  npwq= ngk(ikq)
  ! <
  !
  ! dpsi contains the   perturbed wavefunctions of this k point
  ! evc  contains the unperturbed wavefunctions of this k point
  !
  wgt = 2.d0 * weight / omega
  !
  psir (:,:) = (0.d0, 0.d0)
  do ibnd = 1, nbnd_occ (ikk)
     do ig = 1, npw
!        psir (nls (igk (ig) ), ibnd ) = evc (ig, ibnd)
        psir (nls (igk_k (ig, ikk) ), ibnd ) = evc (ig, ibnd)
     enddo
     CALL invfft ('Wave', psir (1:dffts%nnr, ibnd), dffts)
  enddo
  !
  summ = (0.d0, 0.d0)
  !
  ps   = (0.d0, 0.d0)
  !
  nbnd_eff=nbnd
  ! zgemm will calc. all values of ps w.r.t different ibnd and jbnd 
  CALL zgemm( 'C', 'N', nbnd_occ(ikq), nbnd_occ (ikq), npwq, (1.d0,0.d0), &
               dpsi_plus, npwq, dpsi_minus, npwq, (0.d0,0.d0), ps, nbnd )
  ! 
#if defined __MPI
   call mp_sum(ps(:,1:nbnd_eff),intra_pool_comm)
#endif
  !
  do ibnd = 1,  nbnd_occ (ikk)
     !
     do jbnd = 1, nbnd_occ (ikk)
       !
       ps_1 = ps (ibnd,jbnd) 
       !
       do ir = 1, dffts%nnr
          !
          summ (ir) = summ(ir) + wgt * ps_1 * CONJG(psir (ir, jbnd) ) * psir(ir, ibnd)
          !
       enddo
       !
    end do
    !
  enddo
  !
  deallocate (psir)
  deallocate (ps)

  call stop_clock ('incd_dpsi2')
  return
end subroutine incd_dpsi2
