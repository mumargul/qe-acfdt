#!/bin/bash
#
# files in install
#
list=""
for file in ${list}; do
   filedest="../../install/${file}"
   cmp ${file} ${filedest} 1> /dev/null
   if [ $? -eq 0 ]; then
      echo "${filedest} is not modified"
   else
      echo "${filedest} IS DIFFERENT"
   fi
done
#
# files in Modules
#
list="xml_io_base.f90"
for file in ${list}; do
   filedest="../../Modules/${file}"
   cmp ${file} ${filedest} 1> /dev/null
   if [ $? -eq 0 ]; then
      echo "${filedest} is not modified"
   else
      echo "${filedest} IS DIFFERENT"
   fi
done
#
# files in PW/src
#
list="non_scf.f90 setup.f90 punch.f90 exx.f90"
for file in ${list}; do
   filedest="../../PW/src/${file}"
   cmp ${file} ${filedest} 1> /dev/null
   if [ $? -eq 0 ]; then
      echo "${filedest} is not modified"
   else
      echo "${filedest} IS DIFFERENT"
   fi
done
#
# files in PHonon/PH
#
list="phq_setup.f90 check_initial_status.f90"
for file in ${list}; do
   filedest="../../PHonon/PH/${file}"
   cmp ${file} ${filedest} 1> /dev/null
   if [ $? -eq 0 ]; then
      echo "${filedest} is not modified"
   else
      echo "${filedest} IS DIFFERENT"
   fi
done
#
echo ""
